module.exports = {
  printWidth: 100,
  trailingComma: 'es5',
  useTab: false,
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  endOfLine: 'lf',
  noExtraParens: true,
  arrowParens: 'avoid',
}
