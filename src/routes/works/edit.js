import React from 'react'
import WorkEdit from './WorkEdit'
import Layout from '../../components/Layout/Layout'

async function action({ fetch, params }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `query($id: Int!) {
        work(id: $id) {
          id
          title
          category
          year
          address
          description
          isPublished
          path
          isPortrait
          createdAt
          updatedAt

          blocks {
            id
            body
            path
            isPortrait
          }
        }
      }`,
      variables: {
        id: parseInt(params.id, 10),
      }
    }),
  })

  const { data } = await resp.json()

  if (!data || !data.work) throw new Error('Failed to load the work feed.')
  return {
    title: 'ニュース',
    chunks: ['work'],
    component: (
      <Layout>
        <WorkEdit work={data.work} />
      </Layout>
    ),
  }
}

export default action
