import useStyles from 'isomorphic-style-loader/useStyles'
import Link from '../../components/Link/Link'
import React from 'react'
import PropTypes from 'prop-types'
import s from './WorkList.css'
import Storage from '../../lib/Storage'
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid } from '@material-ui/core'

export default function WorkList({ workList }) {
  useStyles(s)
  return (
    <>
      <h1>Works</h1>

      <Grid container spacing={3}>
        <Grid item xs={6} sm={4}>
          <Card>
            <CardActionArea>
              <div className={s.media} />
              <CardContent>
                New item
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Link className={s.link} to="/admin/works/create">
                <Button size="small" color="primary">
                  Create
                </Button>
              </Link>
            </CardActions>
          </Card>
        </Grid>

        {workList.map(work => (
          <Grid key={work.id} item xs={6} sm={4}>
            <Card>
              <CardActionArea>
                <CardMedia
                  className={s.media}
                  image={Storage.path(work.path)}
                />
                <CardContent>
                  <h2 className={s.title}>{work.title}</h2>
                  <p className={s.description}>{work.description}</p>
                  <p>({work.year})</p>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Link className={s.link} to={'/admin/works/' + work.id}>
                  <Button size="small" color="primary">
                    Edit
                  </Button>
                </Link>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Button fullWidth component={Link} to="/admin">
        Back
      </Button>
    </>
  )
}

WorkList.propTypes = {
  workList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      content: PropTypes.string,
    })

  ).isRequired,
}
