import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import s from './WorkEditPreview.css'

export default function WorkEditPreview({ state }) {
  const previewImage = ({ src, isPortrait }) => {
    return src && (isPortrait
      ? (<div className={[s.image, s.isPortrait].join(' ')}>
        <figure style={{ backgroundImage: `url(${src})` }} />
      </div>)
      : <img className={s.image} src={src} />
    )
  }

  useStyles(s)
  return (
    <article className={s.root}>
      <div className={s.surface}>
        <div>
          {previewImage(state)}
        </div>
        <h1>
          {state.title}
          {!state.isPublished && <span>(非公開)</span>}
        </h1>
        <div className={s.box}>
          {state.year && (<div>
            {state.year}年竣工
          </div>)}
          <div>
            {state.address}
          </div>
          <div className={s.description}>
            {state.description}
          </div>
        </div>
        {state.blocks.map(block => (<div className={s.block}>
          {previewImage(block)}
          <div className={s.blockBody}>{block.body}</div>
        </div>))}
      </div>
    </article>
  )
}
