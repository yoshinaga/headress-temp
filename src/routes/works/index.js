import React from 'react'
import WorkList from './WorkList'
import Layout from '../../components/Layout/Layout'

async function action({ fetch }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `{ workList {
        id title description year path updatedAt
      }}`,
    }),
  })
  const { data } = await resp.json()

  if (!data || !data.workList) throw new Error('Failed to load the work feed.')
  return {
    title: '作品',
    chunks: ['work'],
    component: (
      <Layout>
        <WorkList workList={data.workList} fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
