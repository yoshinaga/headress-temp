import React from 'react'
import WorkEdit from './WorkEdit'
import Layout from '../../components/Layout/Layout'

async function action({ fetch, params }) {
  return {
    title: 'ニュース',
    chunks: ['work'],
    component: (
      <Layout>
        <WorkEdit fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
