import useStyles from 'isomorphic-style-loader/useStyles'
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Link from '../../components/Link/Link'
import WorkEditPreview from './WorkEditPreview'
import UploadedImage from '../../components/UploadedImage'
import InputFile from '../../components/InputFile/InputFile'
import WorkBlockMenu from '../../components/WorkBlockMenu/WorkBlockMenu'
import Storage from '../../lib/Storage'
import deepClone from '../../lib/deepClone'
import { FormControl, TextField, Switch, Button, Card, CardContent, CardActions, Grid } from '@material-ui/core'
import { updateDeepState } from '../../components/State'
import { WorkBlockType } from '../../data/works/WorkTypes'
import { AddCircle, MoreHoriz } from '@material-ui/icons';
import { gqlAction } from '../../lib/gqlFetch'
import { withSnackbar } from 'notistack'

class WorkEdit extends React.Component {
  constructor(props) {
    super(props)

    const defaultProps = {
      year: 2000,
      address: '',
      description: '',
    }

    // デフォルト値の設定
    for (const [key, defaultProp] of Object.entries(defaultProps)) {
      if (props.work[key] === null || props.work[key] === null) {
        props.work[key] = defaultProp
      }
    }
    if (!props.work.blocks || !props.work.blocks.length) {
      props.work.blocks = [this.createBlock]
    }

    // プレビュー画像の設定
    props.work.src = Storage.path(props.work.path)
    props.work.blocks.forEach((block) => {
      block.src = Storage.path(block.path)
    })

    this.state = {
      ...props.work,
      focusedBlockIndex: props.work.blocks.length - 1,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleChangeBoolean = this.handleChangeBoolean.bind(this)
    this.handleChangeFile = this.handleChangeFile.bind(this)
    this.handleFocusBlock = this.handleFocusBlock.bind(this)
    this.submit = this.submit.bind(this)
    this.appendBlock = this.appendBlock.bind(this)
    this.appendBlockPrev = this.appendBlockPrev.bind(this)
    this.moveBlockNext = this.moveBlockNext.bind(this)
    this.moveBlockPrev = this.moveBlockPrev.bind(this)
    this.removeBlock = this.removeBlock.bind(this)
    this.renderBlockEdit = this.renderBlockEdit.bind(this)
  }

  /**
   * 新規Blockオブジェクトを作成
   */
  createBlock() {
    // ブロックテンプレートを作成
    if (!this.blockTemplate_) {
      this.blockTemplate_ = {}
      const fields = Object.keys(WorkBlockType.getFields())
      fields.forEach(field => {
        let value = null
        switch (field) {
          case 'isPortrait':
            value = false
            break
          case 'displayOrder':
            value = ''
            break
          default:
            value = ''
        }
        this.blockTemplate_[field] = value
      })
    }

    // コピーを返す
    return deepClone(this.blockTemplate_)
  }

  /**
   * 情報の送信
   */
  async submit() {
    await gqlAction({
      files: true,
      query: `mutation ($id: Int $input: WorkInput!) {
        upsertWork(id: $id input: $input) {
          id
          blocks {
            id
          }
        }
      }`,
      variables: {
        id: this.state.id ? parseInt(this.state.id, 10) : null,
        input: {
          title: this.state.title,
          category: this.state.category,
          year: parseInt(this.state.year, 10),
          description: this.state.description,
          address: this.state.address,
          isPublished: this.state.isPublished,
          file: this.state.file,
          isPortrait: this.state.isPortrait,
          blocks: this.state.blocks.map((block) => {
            return {
              id: parseInt(block.id, 10),
              body: block.body,
              file: block.file,
              isPortrait: block.isPortrait,
            }
          }),
        },
      },
      action: {
        notify: this.props.enqueueSnackbar,
        message: `${this.state.title}を保存しました`,
        then: (data) => {
          // StateのIDをデータベースと合わせる
          this.setState({ id: data.upsertWork.id })

          const blocks = this.state.blocks.map((block, index) => {
            const dataBlock = data.upsertWork.blocks[index]
            block.id = dataBlock.id || null
            return block
          })
          this.setState({ blocks })
        }
      },
    })
  }

  /**
   * ブロックを追加
   */
  appendBlock(index, diff = 1) {
    index = typeof index === 'number'
      ? index
      : index.currentTarget.dataset.index || 0
    this.state.blocks.splice(index + diff, 0, this.createBlock())
    this.setState({ blocks: this.state.blocks })
  }

  /**
   * ブロックを前に追加
   */
  appendBlockPrev(index, diff = -1) {
    this.appendBlock(index, diff)
  }

  moveBlock(index, diff = 1) {
    const block = this.state.blocks[index]
    this.state.blocks[index] = this.state.blocks[index + diff]
    this.state.blocks[index + diff] = block
    this.setState({ blocks: this.state.blocks })
  }

  moveBlockNext(index) {
    this.moveBlock(index, 1)
  }

  /**
   * ブロックを前に移動
   */
  moveBlockPrev(index) {
    this.moveBlock(index, -1)
  }

  /**
   * ブロックを削除
   */
  removeBlock(index) {
    this.state.blocks.splice(index, 1)
    this.setState({ blocks: this.state.blocks })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: updateDeepState(this.state, event.target.name, event.target.value) })
  }

  handleChangeBoolean(event) {
    this.setState({ [event.target.name]: event.target.checked })
  }

  handleChangeFile(event) {
    const file = event.target.files.item(0) || null
    const uploadedImage = new UploadedImage()

    // Stateの変更先を一階層上にする
    const parentStateName = event.target.name.split('.').slice(0, -1).join('.')

    uploadedImage.onUploaded = ({ imageBase64, imagePreview, isPortrait, isOversize }) => {
      this.setState(updateDeepState(this.state, parentStateName, {
        file,
        src: imagePreview,
        isPortrait,
      }))
    }
    uploadedImage.setImage(event)
  }

  handleFocusBlock(event) {
    this.setState({ focusedBlockIndex: event.currentTarget.dataset.index })
  }

  renderBlockEdit(block, index) {
    return (
      <Card key={index} data-index={index} onMouseDown={this.handleFocusBlock}>
        <CardContent>
          {index + 1}
          <InputFile
            name={`blocks.${index}.file`}
            label="Image"
            backgroundImage={this.state.blocks[index].src}
            onChange={this.handleChangeFile}
          />
          <FormControl fullWidth>
            <TextField
              name={`blocks.${index}.body`}
              label="Caption"
              value={block.body}
              onChange={this.handleChange}
            />
          </FormControl>
        </CardContent>
        {this.state.focusedBlockIndex == index && (<CardActions>
          <Button data-index={index} size="small" onClick={this.appendBlock} >
            <AddCircle />下に追加
          </Button>
          <WorkBlockMenu
            index={index}
            actions={{
              '上に移動': index > 0 && this.moveBlockPrev,
              '下に移動': index < this.state.blocks.length - 1 && this.moveBlockNext,
              '上に追加': this.appendBlockPrev,
              '削除': this.removeBlock,
            }}
          >
            <MoreHoriz />操作
          </WorkBlockMenu>
        </CardActions>)}
      </Card>
    )
  }

  render() {
    return (
      <Grid container>
        <Grid item xs={12} sm={8} md={6}>
          <h1>Works</h1>
          <article>
            <FormControl fullWidth>
              <TextField
                name="title"
                label="Title"
                required={true}
                value={this.state.title}
                onChange={this.handleChange}
              />
            </FormControl>

            <FormControl fullWidth>
              <TextField
                name="category"
                label="Category"
                required={true}
                value={this.state.category}
                onChange={this.handleChange}
              />
            </FormControl>

            <FormControl fullWidth>
              <label>
                <Switch
                  checked={this.state.isPublished}
                  name="isPublished"
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                  onChange={this.handleChangeBoolean}
                />
                公開する
              </label>
            </FormControl>

            <InputFile
              name="file"
              label="Main image"
              backgroundImage={this.state.src}
              onChange={this.handleChangeFile}
            />

            <FormControl fullWidth>
              <TextField
                name="description"
                label="Description"
                helperText="記事の説明"
                multiline
                required={true}
                rows={4}
                value={this.state.description}
                error={false}
                onChange={this.handleChange}
              />
            </FormControl>

            <FormControl fullWidth>
              <TextField
                name="address"
                label="Address"
                value={this.state.address}
                onChange={this.handleChange}
              />
            </FormControl>

            <FormControl fullWidth>
              <TextField
                name="year"
                label="Year of completion"
                value={this.state.year}
                onChange={this.handleChange}
              />
            </FormControl>

            {this.state.blocks.map(this.renderBlockEdit)}

            <FormControl fullWidth>
              <Button color="primary" onClick={this.submit}>
                Save
              </Button>
            </FormControl>
            <FormControl fullWidth>
              <Button fullWidth component={Link} to="/admin/works">
                Back
              </Button>
            </FormControl>
          </article>
        </Grid>
        <Grid item xs={12} sm={4} md={6}>
          <WorkEditPreview state={this.state} />
        </Grid>
      </Grid>
    )
  }
}

WorkEdit.propTypes = {
  fetch: PropTypes.func,
  work: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    year: PropTypes.year,
    description: PropTypes.string,
    isPublished: PropTypes.boolean,
    blocks: PropTypes.array
  })
}

WorkEdit.defaultProps = {
  work: {}
}

export default withSnackbar(WorkEdit)
