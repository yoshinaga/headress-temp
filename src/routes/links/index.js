import React from 'react'
import LinkList from './LinkList'
import Layout from '../../components/Layout'

async function action({ fetch }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `{
        linkList {
          id,title,description,updatedAt
        }
      }`,
    }),
  })
  const { data } = await resp.json()

  if (!data || !data.linkList) throw new Error('Failed to load the link feed.')
  return {
    title: 'リンク',
    chunks: ['link'],
    component: (
      <Layout>
        <LinkList linkList={data.linkList} fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
