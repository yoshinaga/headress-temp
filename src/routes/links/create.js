import React from 'react'
import LinkEdit from './LinkEdit'
import Layout from '../../components/Layout'

async function action({ fetch, params }) {
  return {
    title: 'Links',
    chunks: ['link'],
    component: (
      <Layout>
        <LinkEdit fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
