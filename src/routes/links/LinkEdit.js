import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import Link from '../../components/Link/Link'
import { gqlAction } from '../../lib/gqlFetch'
import { FormControl, FormLabel, TextField, Switch, Button } from '@material-ui/core'
import { withSnackbar } from 'notistack'

class LinkEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      ...props.link,
    }
    this.fetch = props.fetch

    this.handleChange = this.handleChange.bind(this)
    this.handleChangeBoolean = this.handleChangeBoolean.bind(this)
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.submit = this.submit.bind(this)
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleChangeBoolean(event) {
    this.setState({ [event.target.name]: event.target.checked })
  }

  handleChangeDate(date) {
    this.setState({ date })
  }

  /**
   * 情報の送信
   */
  async submit() {
    await gqlAction({
      query: `mutation($id: Int $input: LinkInput!) {
          upsertLink(id: $id input: $input) {
            id
          }
        }`,
      variables: {
        id: this.state.id ? parseInt(this.state.id, 10) : null,
        input: {
          title: this.state.title,
          date: this.state.date,
          category: this.state.category,
          description: this.state.description,
          url: this.state.url,
          isPublished: this.state.isPublished,
        }
      },
      action: {
        notify: this.props.enqueueSnackbar,
        message: `設定を保存しました`,
        then: (data) => {
          // StateのIDをデータベースと合わせる
          this.setState({ id: data.upsertLink.id })
        }
      },
    })
  }


  render() {
    return (
      <>
        <h1>Links</h1>
        <article>
          <FormControl fullWidth>
            <TextField name="title" label="タイトル" value={this.state.title} onChange={this.handleChange} />
          </FormControl>

          <FormControl fullWidth>
            <label>
              <Switch
                checked={this.state.isPublished}
                name="isPublished"
                onChange={this.handleChangeBoolean}
              />
              公開する
            </label>
          </FormControl>

          <FormControl fullWidth>
            <TextField name="category" label="カテゴリ" value={this.state.category} onChange={this.handleChange} />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              name="description"
              label="説明文"
              required={true}
              multiline
              rows={4}
              value={this.state.description}
              error={false}
              helperText="TEST"
              onChange={this.handleChange}
            />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              name="url"
              label="URL"
              value={this.state.url}
              variant="outlined"
              onChange={this.handleChange}
            />
          </FormControl>

          <FormControl fullWidth>
            <Button type="submit" color="primary" onClick={this.submit}>
              save
            </Button>
          </FormControl>
          <FormControl fullWidth>
            <Button component={Link} to="/admin/links">
              back
            </Button>
          </FormControl>
        </article>
      </>
    )
  }
}

// LinkEdit.propTypes = {
//   link: PropTypes.arrayOf(
//     PropTypes.shape({
//       title: PropTypes.string.isRequired,
//       isPublished: PropTypes.boolean,
//     })
//   ).isRequired,
// }

export default withSnackbar(LinkEdit)