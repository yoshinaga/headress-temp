import React from 'react'
import LinkEdit from './LinkEdit'
import Layout from '../../components/Layout'

async function action({ fetch, params }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `query($id: Int!) {
        link(id: $id) {
          id
          title
          category
          url
          isPublished
          updatedAt
        }
      }`,
      variables: {
        id: parseInt(params.id, 10),
      }
    }),
  })
  const { data } = await resp.json()

  if (!data || !data.link) throw new Error('Failed to load the link feed.')
  return {
    title: 'ニュース',
    chunks: ['link'],
    component: (
      <Layout>
        <LinkEdit link={data.link} fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
