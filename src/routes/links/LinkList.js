import useStyles from 'isomorphic-style-loader/useStyles'
import Link from '../../components/Link/Link'
import React from 'react'
import PropTypes from 'prop-types'
import s from './LinkList.css'
import Storage from '../../lib/Storage'
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid } from '@material-ui/core'

export default function LinkList({ linkList }) {
  useStyles(s)
  return (
    <>
      <h1>Links</h1>

      <Grid container spacing={3}>
        <Grid item xs={6} sm={4}>
          <Card>
            <CardActionArea>
              <div className={s.media} />
              <CardContent>
                New item
                </CardContent>
            </CardActionArea>
            <CardActions>
              <Link className={s.link} to="/admin/links/create">
                <Button size="small" color="primary">
                  Create
                  </Button>
              </Link>
            </CardActions>
          </Card>
        </Grid>

        {linkList.map(link => (
          <Grid key={link.id} item xs={6} sm={4}>
            <Card>
              <CardActionArea>
                <CardContent>
                  <h2 className={s.title}>{link.title}</h2>
                  <p className={s.description}>{link.description}</p>
                  <p>({link.category})</p>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Link className={s.link} to={`/admin/links/${link.id}`}>
                  <Button size="small" color="primary">
                    Edit
                  </Button>
                </Link>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Button fullWidth component={Link} to="/admin">
        Back
      </Button>
    </>
  )
}

LinkList.propTypes = {
  linkList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.string,
      content: PropTypes.string,
    })
  ).isRequired,
}
