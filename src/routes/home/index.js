import React from 'react'
import Home from './Home'
import Layout from '../../components/Layout'
import { gqlFetch } from '../../lib/gqlFetch'

async function action({ fetch }) {
  const pages = [
    // {
    //   name: 'news',
    //   description: 'ニュースを編集します'
    // },
    {
      name: 'works',
      description: '記事を編集します'
    },
    {
      name: 'links',
      description: '関連サイトを編集します'
    },
    {
      name: 'setting',
      description: 'サイト設定を変更します'
    },
  ]

  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `query($key: String!) {
        setting(key: $key) {
          value updatedAt
        }
        changedContents {
          type name createdAt updatedAt
        }
      }`,
      variables: {
        key: 'publishedAt'
      }
    }),
  })
  const { data } = await resp.json()
  const changedContents = data.changedContents || []
  const publishedAt = data.setting ? data.setting.value : ''

  // const { data, errors } = await gqlFetch({
  //   query: `{
  //     changedContents {
  //       type name createdAt updatedAt
  //     }
  //   }`,
  // })
  // const changed = data.changedContents || []

  return {
    title: 'admin',
    chunks: ['home'],
    component: (
      <Layout>
        <Home changedContents={changedContents} pages={pages} publishedAt={publishedAt} />
      </Layout>
    ),
  }
}

export default action
