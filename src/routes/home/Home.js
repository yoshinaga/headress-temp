import useStyles from 'isomorphic-style-loader/useStyles'
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import s from './Home.css'
import Link from '../../components/Link/Link'
import { Card, CardActionArea, CardActions, CardContent, CardMedia, Grid } from '@material-ui/core'
import PublishButton from '../../components/PublishButton'

export default function Home({ pages, changedContents, publishedAt }) {
  const [changed, setChanged] = useState(changedContents)
  const clearChanged = () => {
    setChanged([])
  }

  useStyles(s)
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Card>
          {/* <CardMedia
            className={s.media}
            image={Storage.path(work.path)}
          /> */}
          <CardContent>
            {publishedAt || '-'}
            {changed.length && (
              <h2 className={s.title}>未公開の更新が{changed.length}件あります</h2>
            )}
            {changed.map((content, index) => (<div key={index}>
              {content.type} {content.name}
            </div>))}
          </CardContent>
          <CardActions>
            <PublishButton onSuccess={clearChanged} />
          </CardActions>
        </Card>
      </Grid>
      {pages.map(page => (
        <Grid key={page.name} item xs={12} sm={4}>
          <Link className={s.link} to={'/admin/' + page.name}>
            <Card>
              <CardActionArea>
                {/* <CardMedia
                  className={s.media}
                  image={Storage.path(work.path)}
                /> */}
                <CardContent>
                  <h2 className={s.title}>{page.name}</h2>
                  <p className={s.description}>{page.description}</p>
                </CardContent>
              </CardActionArea>
            </Card>
          </Link>
        </Grid>
      ))}
    </Grid>
  )
}

Home.propTypes = {
  changed: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  pages: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string,
    })
  ).isRequired,
}

Home.defaultProps = {
  changed: [],
}
