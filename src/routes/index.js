/* eslint-disable global-require */

// The top-level (parent) route
const routes = {
  path: '/admin',

  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '',
      load: () => import(/* webpackChunkName: 'home' */ './home'),
    },
    {
      path: '/contact',
      load: () => import(/* webpackChunkName: 'contact' */ './contact'),
    },
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'login' */ './login'),
    },
    {
      path: '/register',
      load: () => import(/* webpackChunkName: 'register' */ './register'),
    },
    {
      path: '/about',
      load: () => import(/* webpackChunkName: 'about' */ './about'),
    },
    {
      path: '/privacy',
      load: () => import(/* webpackChunkName: 'privacy' */ './privacy'),
    },
    {
      path: '/admin',
      load: () => import(/* webpackChunkName: 'admin' */ './admin'),
    },
    {
      path: '/feed',
      load: () => import(/* webpackChunkName: 'feed' */ './feed'),
    },
    {
      path: '/news',
      children: [
        {
          path: '',
          load: () => import(/* webpackChunkName: 'news' */ './news'),
        },
        {
          path: '/:id',
          load: () => import('./news/edit'),
        },
      ],
    },
    {
      path: '/works',
      children: [
        {
          path: '',
          load: () => import(/* webpackChunkName: 'work' */ './works'),
        },
        {
          path: '/create',
          load: () => import('./works/create'),
        },
        {
          path: '/:id',
          load: () => import('./works/edit'),
        },
      ],
    },
    {
      path: '/links',
      children: [
        {
          path: '',
          load: () => import(/* webpackChunkName: 'link' */ './links'),
        },
        {
          path: '/create',
          load: () => import('./links/create'),
        },
        {
          path: '/:id',
          load: () => import('./links/edit'),
        },
      ],
    },
    {
      path: '/setting',
      children: [
        {
          path: '',
          load: () => import(/* webpackChunkName: 'setting' */ './setting'),
        },
      ],
    },

    // Wildcard routes, e.g. { path: '(.*)', ... } (must go last)
    {
      path: '(.*)',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next()

    // Provide default values for title, description etc.
    route.title = `${route.title || 'Untitled Page'} - Headless cms`
    route.description = route.description || ''

    return route
  },
}

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  })
}

export default routes
