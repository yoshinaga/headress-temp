import React from 'react'
import SettingEdit from './SettingEdit'
import Layout from '../../components/Layout'

async function action({ fetch }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `{
        settings {
          key value
        }
      }`,
    }),
  })

  const { data, errors } = await resp.json()
  if (!data || !data.settings) throw new Error(errors)
  return {
    title: 'システム設定',
    chunks: ['setting'],
    component: (
      <Layout>
        <SettingEdit settings={data.settings} />
      </Layout>
    ),
  }
}

export default action
