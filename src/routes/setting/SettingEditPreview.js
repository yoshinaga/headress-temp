import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import s from './SettingEditPreview.css'

export default function WorkEditPreview({ state }) {
  const parse = ({ key, value }) => {
    switch (key) {
      case 'profile':
        // react-string-replaceでstringからDOMに変換
        break
    }
    return <>{value}</>
  }

  useStyles(s)
  return (
    <article className={s.root}>
      <div className={s.surface}>
        {state.settings.map(setting => (<div className={s.block}>
          <h2>Profile</h2>
          <div className={s.blockBody}>{parse(setting)}</div>
        </div>))}
      </div>
    </article>
  )
}
