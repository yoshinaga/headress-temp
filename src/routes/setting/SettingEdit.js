import useStyles from 'isomorphic-style-loader/useStyles'
import PropTypes from 'prop-types'
import React from 'react'
import { FormControl, TextField, Button, Grid } from '@material-ui/core'
import Link from '../../components/Link/Link'
import { gqlAction } from '../../lib/gqlFetch'
import { withSnackbar } from 'notistack'
import SettingEditPreview from './SettingEditPreview'

class SettingEdit extends React.Component {
  constructor(props) {
    super(props)
    this.attributes = {
      profile: {
        type: 'textarea',
        description: 'Profile',
        help: 'Aboutに表示されるプロフィール設定',
      },
    }
    this.state = {
      settings: this.initSettingsFromAttributes(props.settings),
    }

    this.handleSettingChange = this.handleSettingChange.bind(this)
    this.submit = this.submit.bind(this)
  }

  /**
   * 必要な設定値のみStateに格納する
   *
   * @param {*} settingsProp GraphQLクエリから受け取ったSettingリスト
   */
  initSettingsFromAttributes(settingsProp) {
    let settings = []
    settingsProp.forEach((settingProp) => {
      if (this.attributes[settingProp.key]) {
        settings.push(settingProp)
      }
    })

    // 登録のない項目がある場合は新規登録
    if (Object.keys(settings).length < Object.keys(this.attributes).length) {
      const newSettings = []
      Object.entries(this.attributes).forEach(([key, attribute]) => {
        const setting = settings.find((setting) => setting.key === key)
        const value = setting ? setting[key] : ''
        newSettings.push({
          key,
          value,
        })
      })
      settings = newSettings
    }

    return settings
  }

  handleSettingChange(event) {
    const settings = this.state.settings.slice();
    settings[event.target.name].value = event.target.value
    this.setState({ settings })
  }

  /**
   * 情報の送信
   */
  async submit() {
    await gqlAction({
      query: `mutation($input: [SettingInput]!) {
        upsertSettings(input: $input) {
          key
          value
        }
      }`,
      variables: {
        input: this.state.settings
      },
      action: {
        notify: this.props.enqueueSnackbar,
        message: `設定を保存しました`,
      },
    })
  }

  render() {
    return (
      <Grid container>
        <Grid item xs={12} sm={8} md={6}>
          <h1>Setting</h1>
          <article>
            {this.state.settings.map(({ key, value }, index) => (
              <React.Fragment key={key}>
                <FormControl fullWidth>
                  {key}
                  <TextField
                    name={index}
                    label={this.attributes[key].description}
                    required={true}
                    multiline
                    rows={6}
                    value={this.state.settings[index].value}
                    helperText={this.attributes[key].help}
                    onChange={this.handleSettingChange}
                  />
                </FormControl>
              </React.Fragment>
            ))}

            <FormControl fullWidth>
              <Button type="submit" color="primary" onClick={this.submit}>
                Save
              </Button>
            </FormControl>
            <FormControl fullWidth>
              <Button component={Link} to="/admin">
                Back
              </Button>
            </FormControl>
          </article>
        </Grid>
        <Grid item xs={12} sm={4} md={6}>
          <SettingEditPreview state={this.state} />
        </Grid>
      </Grid>
    )
  }
}

SettingEdit.propTypes = {
  settings: PropTypes.array,
}

SettingEdit.defaultProps = {
  work: []
}

export default withSnackbar(SettingEdit)
