import React from 'react'
import Feed from './Feed'
import Layout from '../../components/Layout'

async function action({ fetch }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: '{feed{title,link,content}}',
    }),
  })
  const { data } = await resp.json()
  if (!data || !data.feed) throw new Error('Failed to load the feed feed.')
  return {
    title: 'ニュース',
    chunks: ['feed'],
    component: (
      <Layout>
        <Feed feed={data.feed} />
      </Layout>
    ),
  }
}

export default action
