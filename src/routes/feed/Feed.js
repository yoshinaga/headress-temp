import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import PropTypes from 'prop-types'
import s from './Feed.css'

export default function Feed({ feed }) {
  useStyles(s)
  return (
    <div className={s.root}>
      <div className={s.container}>
        <h1>React.js Feed</h1>
        {feed.map(item => (
          <article key={item.link} className={s.feedItem}>
            <h1 className={s.feedTitle}>
              <a href={item.link}>{item.title}</a>
            </h1>
            <div
              className={s.feedDesc}
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: item.content }}
            />
          </article>
        ))}
      </div>
    </div>
  )
}

Feed.propTypes = {
  feed: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
      content: PropTypes.string,
    })
  ).isRequired,
}
