import React from 'react'
import NewsEdit from './NewsEdit'
import Layout from '../../components/Layout'

async function action({ fetch, params }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `query($id: Int!) {
        news(id: $id) {
          id
          title
          category
          date
          body
          url
          isPublished
          updatedAt
        }
      }`,
      variables: {
        id: parseInt(params.id, 10),
      }
    }),
  })
  const { data } = await resp.json()

  if (!data || !data.news) throw new Error('Failed to load the news feed.')
  return {
    title: 'ニュース',
    chunks: ['news'],
    component: (
      <Layout>
        <NewsEdit news={data.news} fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
