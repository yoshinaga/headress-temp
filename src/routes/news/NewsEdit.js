import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import { FormControl, FormLabel, TextField, Switch, Button, Box, Checkbox, Radio, List, ListItem, ListItemText } from '@material-ui/core'
import { gqlAction } from '../../lib/gqlFetch'

import {
  DatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers"
import "dayjs/locale/nl"
import DayjsUtils from "@date-io/dayjs"
import dayjs from 'dayjs'
import Link from '../../components/Link/Link'

export default class NewsEdit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props.news,
    }
    this.fetch = props.fetch

    this.handleChange = this.handleChange.bind(this)
    this.handleChangeBoolean = this.handleChangeBoolean.bind(this)
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.submit = this.submit.bind(this)
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleChangeBoolean(event) {
    this.setState({ [event.target.name]: event.target.checked })
  }

  handleChangeDate(date) {
    this.setState({ date })
  }

  /**
   * 情報の送信
   */
  async submit() {
    await gqlAction({
      files: false,
      query: `mutation($id: Int! $input: NewsInput!) {
          updateNews(id: $id input: $input) {
            id
          }
        }`,
      variables: {
        id: parseInt(this.state.id, 10),
        input: {
          title: this.state.title,
          date: this.state.date,
          category: this.state.category,
          body: this.state.body,
          url: this.state.url,
          isPublished: this.state.isPublished,
        }
      },
      action: {
        notify: () => {},
        message: `${this.state.title}を保存しました`,
      },
    })

    const body = JSON.stringify({
      query: `mutation($id: Int! $input: NewsInput!) {
          updateNews(id: $id input: $input) {
            id
          }
        }`,
      variables: {
        id: parseInt(this.state.id, 10),
        input: {
          title: this.state.title,
          date: this.state.date,
          category: this.state.category,
          body: this.state.body,
          url: this.state.url,
          isPublished: this.state.isPublished,
        }
      }
    })
    const resp = await this.fetch('/admin/graphql', { body })

    const { errors } = await resp.json()
  }

  sanitize() {
    const specialChars = {
      "\\": '&#92;',
      "&#92;n": "\n",
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      '&': '&amp;',
      "\'": '&#39;',
      "\0": '',
    }

    specialChars.forEach((char) => {

    })
  }

  render() {
    return (
      <>
        <h1>News / Edit</h1>
        <article>
          <FormControl fullWidth>
            <TextField name="title" label="タイトル" value={this.state.title} onChange={this.handleChange} />
          </FormControl>

          <FormControl fullWidth>
            <label>
              <Switch
                checked={this.state.isPublished}
                name="isPublished"
                onChange={this.handleChangeBoolean}
              />
              公開する
            </label>
          </FormControl>

          <MuiPickersUtilsProvider utils={DayjsUtils}>
            <DatePicker
              name="date"
              label="ニュース日付"
              value={this.state.date}
              onChange={this.handleChangeDate}
              format="YYYY-M-D"
              variant="inline"
            />
          </MuiPickersUtilsProvider>

          <FormControl fullWidth>
            <TextField name="category" label="カテゴリ" value={this.state.category} onChange={this.handleChange} />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              name="body"
              label="本文"
              required={true}
              multiline
              rows={4}
              value={this.state.body}
              error={false}
              helperText="TEST"
              onChange={this.handleChange}
            />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              name="url"
              label="URL"
              value={this.state.url}
              variant="outlined"
              onChange={this.handleChange}
            />
          </FormControl>

          <FormControl fullWidth>
            <Button type="submit" color="primary" onClick={this.submit}>
              save
            </Button>
          </FormControl>
          <FormControl fullWidth>
            <Button component={Link} to="/admin/news">
              back
            </Button>
          </FormControl>
        </article>
      </>
    )
  }
}

// NewsEdit.propTypes = {
//   news: PropTypes.arrayOf(
//     PropTypes.shape({
//       title: PropTypes.string.isRequired,
//       isPublished: PropTypes.boolean,
//     })
//   ).isRequired,
// }
