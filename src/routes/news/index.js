import React from 'react'
import NewsList from './NewsList'
import Layout from '../../components/Layout'

async function action({ fetch }) {
  const resp = await fetch('/admin/graphql', {
    body: JSON.stringify({
      query: `{
        newsList {
          id,title,body,date,updatedAt
        }
      }`,
    }),
  })
  const { data } = await resp.json()

  if (!data || !data.newsList) throw new Error('Failed to load the news feed.')
  return {
    title: 'ニュース',
    chunks: ['news'],
    component: (
      <Layout>
        <NewsList newsList={data.newsList} fetch={fetch} />
      </Layout>
    ),
  }
}

export default action
