import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import PropTypes from 'prop-types'
import s from './NewsList.css'
import Link from '../../components/Link/Link'

export default function NewsList({ newsList }) {
  useStyles(s)
  return (
    <>
      <h1>News</h1>
      {newsList.map(news => (
        <article key={news.id}>
          <h2>
            {news.date}
            <Link className={s.link} to={'/admin/news/' + news.id}>
              {news.title}
            </Link>
          </h2>
          <div className={s.newsDesc}>
            {news.body}
          </div>
        </article>
      ))}
    </>
  )
}

NewsList.propTypes = {
  newsList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.string,
      content: PropTypes.string,
    })
  ).isRequired,
}
