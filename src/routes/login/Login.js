import useStyles from 'isomorphic-style-loader/useStyles'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import s from './Login.css'
import ApplicationContext from '../../components/ApplicationContext'

export default function Login({ title }) {
  useStyles(s)
  const { context } = useContext(ApplicationContext)
  const errors = context.flash.error || []

  return (
    <div className={s.root}>
      <div className={s.container}>
        <h1>{title}</h1>
        {errors.map((error, index) => {
          return <p key={index}>{index} {error}</p>
        })}
        <p className={s.lead}>Log in with your username or company email address.</p>
        <form method="post" action="/admin/login">
          <div className={s.formGroup}>
            <label className={s.label} htmlFor="loginId">
              Username:
              <input
                className={s.input}
                id="loginId"
                type="text"
                name="loginId"
                autoFocus // eslint-disable-line jsx-a11y/no-autofocus
              />
            </label>
          </div>
          <div className={s.formGroup}>
            <label className={s.label} htmlFor="password">
              Password:
              <input className={s.input} id="password" type="password" name="password" />
            </label>
          </div>
          <div className={s.formGroup}>
            <button className={s.button} type="submit">
              Log in
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

Login.propTypes = {
  title: PropTypes.string.isRequired,
}
