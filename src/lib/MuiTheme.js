import { createMuiTheme } from '@material-ui/core/styles'
import { red } from '@material-ui/core/colors'

const defaultTheme = createMuiTheme();

const MuiTheme = createMuiTheme({
  spacing: 8,
  palette: {
    primary: {
      main: '#556446'
    },
    secondary: {
      main: '#19857b'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fefefd'
    }
  },
  props: {
    MuiFormControl: {
      margin: 'dense'
    },
    MuiTextField: {
      variant: 'outlined'
    },
    MuiCheckbox: {
      color: "primary"
    },
    MuiRadio: {
      color: "primary"
    },
    MuiSwitch: {
      color: "primary"
    },
    MuiButton: {
      variant: 'contained'
    },
  },
  mixins: {
    toolbar: {
        minHeight: 42
    }
  },
  card: {
    root: {
      margin: 10,
    },
  },
  overrides: {
    MuiCard: {
      root: {
        marginTop: defaultTheme.spacing(2),
        marginBottom: defaultTheme.spacing(2),
      },
    },
  },
})

export default MuiTheme
