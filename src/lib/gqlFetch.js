import fetch from 'node-fetch'
import FormData from 'form-data'

/**
 * GraphQLリクエストを実行する
 *
 * @param {Object} options
 *   query {Object} JSONに変換してGraphQLにリクエスト
 *   variables {Object} 値
 *   files {Boolean} variables.inputのFileオブジェクトをマッピングするか
 * @returns {Object} { data, errors }
 */
export async function gqlFetch(options) {
  const body = new FormData()
  body.append('operations', JSON.stringify(options))

  if (options.files) {
    const { map, files } = getFileData(options.variables.input, 'variables.input')
    body.append('map', JSON.stringify(map))
    for (const [index, file] of Object.entries(files)) {
      body.append(index, file)
    }
  } else {
    body.append('map', '{}')
  }

  const resp = await fetch('/admin/graphql', { method: 'POST', body });
  return resp.json()
}

/**
 * GraphQLレスポンスに応じて通知を表示する
 *
 * @param {Object} options
 *   query {Object} JSONに変換してGraphQLにリクエスト
 *   variables {Object} 値
 *   files {Boolean} variables.inputのFileオブジェクトをマッピングするか
 *   action {Object} アクション用の値
 */
export async function gqlAction(options) {
  const { action } = options
  delete options.action

  const { data, errors } = await gqlFetch(options)
  if (errors) {
    errors.forEach((error) => {
      action.notify(error.message, { variant: 'error' })
    })
  } else {
    if (action.then) {
      action.then(data)
    }
    action.notify(action.message || '保存しました', { variant: 'success' })
  }
}

/**
 * ファイルオブジェクトが含まれる場合はファイルとマップを取得
 *
 * @param {*} target 走査対象となるオブジェクトや値
 * @param {String} inputKey variables.inputの格納先キー (ドット区切り)
 * @param {Object} data 参照渡しで再起的呼び出し中は共有される
 * @returns {Object}
 */
function getFileData(target, inputKey, data = { map: {}, files: {}, index: -1 }) {
  if (target instanceof File) {
    // ファイルの場合参照渡しのファイル情報を書き換える
    data.index += 1
    data.map[data.index] = [inputKey]
    data.files[data.index] = target
  } else if (target instanceof Array) {
    target.forEach((child, index) => {
      getFileData(child, `${inputKey}.${index}`, data)
    })
  } else if (target instanceof Object) {
    Object.keys(target).forEach((key) => {
      getFileData(target[key], `${inputKey}.${key}`, data)
    })
  }
  return data
}
