/**
 * オブジェクトタイプ取得
 *
 * @param {Object} object
 */
const typeOf = (object) => Object.prototype.toString.call(object).slice(8, -1).toLowerCase()

/**
 * ディープコピー
 *
 * @param {*} object
 * @param {*} existingObjects
 */
const deepClone = (object, existingObjects) => {
  if (!existingObjects) {
    existingObjects = []
  } else if (existingObjects.indexOf(object) !== -1) {
    throw new Error('Recursive reference exists.')
  } else {
    existingObjects = [...existingObjects, object]
  }

  // Array
  if (Array.isArray(object)) {
    return object.map(value => deepClone(value, existingObjects))
  }

  // Object
  if (typeof object === 'object') {
    switch (typeOf(object)) {
      default:
      case 'object': {
        const symbols = Object.getOwnPropertySymbols(object)
        const propNames = Object.getOwnPropertyNames(object)
        const prototype = Object.getPrototypeOf(object)
        return [...propNames, ...symbols].reduce((propertiesObject, propName) => {
          const prop = Object.getOwnPropertyDescriptor(object, propName)
          if (prop.hasOwnProperty('value'))
            prop.value = deepClone(prop.value, existingObjects)
          Object.defineProperty(propertiesObject, propName, prop)
          return propertiesObject
        }, Object.create(prototype))
      }
      case 'number': return new Number(object)
      case 'string': return new String(object)
      case 'boolean': return new Boolean(object)
      case 'bigint': return object.valueOf()
      case 'regexp': return new RegExp(object)
      case 'null': return null
      case 'date': return new Date(object)
      case 'map': {
        const map = new Map()
        for (const [key, value] of object)
          map.set(key, deepClone(value, existingObjects))
        return map
      }
      case 'set': return new Set(object)
    }
  }
  return object
}

export default deepClone