import fs from 'fs'
import mkdirp from 'mkdirp'

const storageDir = 'storage/'

/**
 * ファイルストレージ操作
 */
export default class Storage {
  /**
   * ストレージ内のファイルパスを取得する
   * @param {*} filePath
   */
  static path(filePath) {
    return filePath === null || filePath === undefined
      ? ''
      : `/${storageDir}/${filePath}`
  }

  /**
   * readStreamからStorageディレクトリにファイルを書き込む
   * @param {ReadStream} src
   * @param {String|Array} filePath Storage内ファイルパス
   * @param {string|null} mimetype 指定した場合は拡張子を追加
   */
  static async put(src, filePath, mimetype = null) {
    // ディレクトリを作成
    if (mimetype) filePath += `.${this.getExtension(mimetype)}`
    const storagePath = storageDir + filePath
    const filedir = storagePath.split('/').slice(0, -1).join('/')
    mkdirp.sync(filedir)

    const dest = fs.createWriteStream(storagePath)
    src.pipe(dest)

    return new Promise(function (resolve, reject) {
      src.on('end', () => {
        resolve(true)
      })
      src.on('error', (e) => {
        dest.destroy(e)
        dest.end()
        console.error(e)
        return reject()
      })
      dest.on('error', (e) => {
        console.error(e)
        return reject()
      })
    })
  }

  static async delete(filePath) {
    if (!filePath) {
      return
    }
    return fs.unlink(filePath, (error) => {})
  }

  /**
   * GraphQLUploadオブジェクトからReadStreamと拡張子を取得
   *
   * @param {Object} uploadedFile
   */
  static async parseUploadedFile(uploadedFile) {
    if (!uploadedFile) {
      return {
        src: null,
        extension: null
      }
    }
    const { mimetype, createReadStream } = await uploadedFile
    return {
      src: createReadStream(),
      extension: Storage.getExtension(mimetype)
    }
  }

  /**
   * MIMEタイプからファイル拡張子を取得
   * @param {string} mimetype
   */
  static getExtension(mimetype) {
    // 拡張子判別
    const extensions = {
      'image/jpeg': 'jpg',
      'image/png': 'png',
      'image/gif': 'gif',
    }

    return extensions[mimetype] || null
  }
}