import dayjs from "dayjs"
import { GraphQLError } from "graphql"
import attributes from "../data/attributes"

const attributeTypes = {
  NUMBER: 'number',
  DATETIME: 'datetime',
}

const MAX_LENGTHES = {
  string: 255,
  text: 1000,
}

/**
 * バリデーションルール
 */
export default class Rule {
  /**
   * @param {*} rules
   * {項目名: {ルールタイプ: パラメータ}}
   * @example
   *   { attribute: [ 'required', 'min:4', { max: 10 } ] }
   */
  constructor(rules) {
    this.rules = rules
  }

  /**
   * バリデーション実行
   * @param {Object} input
   */
  getErrorMessages(input) {
    this.input = input
    const errorMessages = {}
    Object.entries(this.rules).some(([key, rules]) => {
      /** 項目別のステータス */
      const attribute = {
        name: this.getAttributeName(key),
        value: this.input[key] || null,
        type: null,
        skip: false,
      }

      rules.some((rule) => {
        try {
          const errorMessage = this.checkRule(rule, attribute)
          if (errorMessage) {
            errorMessages[key] = errorMessage
            attribute.skip = true
          }

          // スキップが有効な場合は次の項目へ
          return attribute.skip
        } catch (e) {
          console.error(e)
          errorMessages[key] = `${attribute.name}でエラーが発生しました`
        }
        return true
      })
    })

    return errorMessages
  }

  validate(input) {
    const errorMessages = this.getErrorMessages(input)
    if (Object.keys(errorMessages).length) {
      throw new GraphQLError(Object.values(errorMessages)[0], null, null, null, null, null, { errorMessages })
    }
  }

  /**
   * バリデーション用メソッドと引数を取得
   *
   * @param {Array} rule
   */
  checkRule(rule, attribute) {
    let ruleType, parameters

    if (rule instanceof Function) {
      // コールバック
      rule.bind(this)
      return rule(attribute, attribute.value, this.input)
    } else {
      // ルールタイプに一致するメソッドを呼び出す
      if (rule instanceof Object) {
        ruleType = Object.keys(rule)[0]
        parameters = rule[ruleType]
      } else {
        const ruleChunks = rule.split(':', 2)
        ruleType = ruleChunks[0]
        parameters = ruleChunks[1] ?? null
      }
      return this[ruleType](attribute, attribute.value, parameters)
    }
  }

  /**
   * 現在バリデーション中の項目名を取得
   *
   * @param {String} key
   * @returns {String}
   */
  getAttributeName(key) {
    return attributes[key] || key
  }

  /** 数値 */
  number(attribute, value, parameters) {
    attribute.type = attributeTypes.NUMBER
    if (!/^\d*$/.test(value)) {
      return `${attribute.name}は数値で入力してください`
    }
  }

  /** 日時 */
  datetime(attribute, value, parameters) {
    attribute.type = attributeTypes.DATETIME

    const matches = value.match(/^(?:[1-9]\d{3})-(?:0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[01]) (?:[0-1]?\d|2[0-3]):(?:[0-5]?\d):(?:[0-5]?\d)$/)
    if (!matches) {
      return `${attribute.name}の形式が正しくありません`
    }

    if (parseInt(matches[1], 10) !== parseInt(dayjs(value, 'YYYY-M-D').format('D'), 10)) {
      return `${attribute.name}に正しい日付を入力してください`
    }
  }

  /** 必須 */
  required(attribute, value, parameters) {
    if (value === null || value === '') {
      return `${attribute.name}を入力してください`
    }
  }

  /** NULL許可 */
  nullable(attribute, value, parameters) {
    if (value === null || value === '') {
      attribute.skip = true
    }
  }

  /** 入力不可 */
  empty(attribute, value, parameters) {
    if (value !== null && value !== '') {
      attribute.skip = true
      return `${attribute.name}が不正です`
    }
  }

  /** 最大文字数 */
  max(attribute, value, parameters) {
    const length = (value ?? '').length
    if (MAX_LENGTHES[parameters]) {
      parameters = MAX_LENGTHES[parameters]
    }
    if (length > parameters) {
      return `${attribute.name}は${parameters}文字以内で入力してください`
    }
  }

  /** 正規表現 */
  regex(attribute, value, parameters) {
    if (!/^\d{3}-\d{4}$/.test(value)) {
      return `${attribute.name}の形式が正しくありません`
    }
  }

  /** メールアドレス */
  email(attribute, value, parameters) {
    if (!/^[\w.!#$%&'*+\/=?^`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) {
      return `${attribute.name}の形式が正しくありません`
    }
  }

  /** 電話番号 */
  phone(attribute, value, parameters) {
    const length = value.length
    if (value.indexOf('-') < 0) {
      return `${attribute.name}はハイフン(-)を含めて入力してください`
    }
    if (length < 12 || length < 13 || !/^0\d{2,3}-\d{1,4}-\d{4}$/.test(value)) {
      return `${attribute.name}の形式が正しくありません`
    }
  }

  /** 郵便番号 */
  zip(attribute, value, parameters) {
    if (value.indexOf('-') < 0) {
      return `${attribute.name}はハイフン(-)を含めて入力してください`
    }
    if (!/^\d{3}-\d{4}$/.test(value)) {
      return `${attribute.name}の形式が正しくありません`
    }
  }

  /** 他の項目より後 */
  after(attribute, value, parameters) {
    const datetime = dayjs(this.input[parameters] || null)
    if (datetime.isValid() && dayjs(value).diff(datetime) <= 0) {
      return `${attribute.name}は${this.getAttributeName(parameters)}よりも後に設定してください`
    }
  }

  /**
   * モデルに入力値を反映する
   * @param {*} model
   * @param {*} input
   */
  fill(model, input) {
    const fillables = {
      Work: [
        'title',
        'category',
        'year',
        'description',
        'address',
        'isPublished',
        'path',
        'extension',
        'isPortrait',
      ],
      WorkBlock: [
        'body',
        'displayOrder',
        'path',
        'extension',
        'isPortrait',
      ],
      Admin: [
        'loginId',
      ],
    }

    const modelName = model.toString().replace(/\[object SequelizeInstance:([^\]]+)\].*$/, '$1')
    const fillable = fillables[modelName]

    fillable.forEach(attribute => {
      if (input[attribute] === undefined) return
      model[attribute] = input[attribute]
    })
  }
}