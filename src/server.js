import path from 'path'
import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import expressJwt, { UnauthorizedError as Jwt401Error } from 'express-jwt'
import { graphql } from 'graphql'
import { graphqlHTTP } from 'express-graphql'
import { graphqlUploadExpress } from 'graphql-upload';
import flash from 'connect-flash'
import nodeFetch from 'node-fetch'
import React from 'react'
import ReactDOM from 'react-dom/server'
import PrettyError from 'pretty-error'
import App from './components/App'
import Html from './components/Html'
import { ErrorPageWithoutStyle } from './routes/error/ErrorPage'
import errorPageStyle from './routes/error/ErrorPage.css'
import createFetch from './lib/createFetch'
import passport from './passport'
import { Strategy as LocalStrategy } from 'passport-local'
import router from './router'
import models from './data/models'
import schema from './data/schema'
// import assets from './asset-manifest.json'; // eslint-disable-line import/no-unresolved
import chunks from './chunk-manifest.json' // eslint-disable-line import/no-unresolved
import config from './config'
import Admin from './data/models/Admin'
import { exec } from 'child_process'
import dayjs from 'dayjs'

require('dotenv').config()

const session = require('express-session')
const methodOverride = require('method-override')
// const csrf = require('csurf')
// const csrfProtection = csrf({ cookie: false })

const loginPath = '/admin/login'

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection at:', p, 'reason:', reason)
  // send entire app down. Process manager will restart it
  process.exit(1)
})

// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {}
global.navigator.userAgent = global.navigator.userAgent || 'all'

const app = express()

//
// If you are using proxy from external machine, you can set TRUST_PROXY env
// Default is to trust proxy headers only from loopback interface.
// -----------------------------------------------------------------------------
app.set('trust proxy', config.trustProxy)

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.resolve(__dirname, 'public')))

// アップロードされたファイル
app.use('/storage', express.static(path.resolve(__dirname, '../storage')))

app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(methodOverride('_method'))

// アプリケーションのセッション設定
app.use(
  session({
    secret: 'secretsecretsecret',
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: true,
  })
)
app.use(flash())

//
// Authentication
// -----------------------------------------------------------------------------
app.use(
  expressJwt({
    secret: config.auth.jwt.secret,
    algorithms: ['HS256'],
    credentialsRequired: false,
    getToken: req => req.cookies.id_token,
  })
)

// Error handler for express-jwt
app.use((err, req, res, next) => {
  // eslint-disable-line no-unused-vars
  if (err instanceof Jwt401Error) {
    console.error('[express-jwt-error]', req.cookies.id_token)
    // `clearCookie`, otherwise user can't use web-app until cookie expires
    res.clearCookie('id_token')
  }
  next(err)
})

// Local Login
app.use(passport.initialize())
app.use(passport.session())
passport.serializeUser((admin, done) => {
  done(null, admin)
})
passport.deserializeUser((admin, done) => {
  done(null, admin)
})
passport.use(new LocalStrategy(
  {
    usernameField: 'loginId',
    passwordField: 'password',
    failureFlash: true,
  },
  async (loginId, password, done) => {
    // ログイン処理
    const admin = await Admin.findOne({ where: { loginId } })
    if (admin && admin.validPassword(password)) {
      return done(null, admin)
    }
    return done(null, false, { message: 'ログインに失敗しました' })
  }
))

// ログイン処理
app.post('/admin/login',
  passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/admin/login',
    failureFlash: true,
  })
)

// 公開処理
app.post('/admin/publish', (req, res) => {
  const proc = exec('cd ../headless_front; yarn reload', async (error, stdout, stderr) => {
    if (!error) {
      const fetch = createFetch(nodeFetch, {
        baseUrl: config.api.serverUrl,
        cookie: req.headers.cookie,
        schema,
        graphql,
      })

      const resp = await fetch('/admin/graphql', {
        body: JSON.stringify({
          query: `mutation($input: SettingInput!) {
            upsertSetting(input: $input) {
              value
              updatedAt
            }
          }`,
          variables: {
            input: {
              key: 'publishedAt',
              value: dayjs().format('YYYY-MM-DD HH:mm:ss')
            }
          }
        }),
      })
      const { data } = await resp.json()
      res.send('data.upsertSetting')
    }

  })
//  res.json({ message: "Hello,world" })
})

//
// Register API middleware
// -----------------------------------------------------------------------------
app.use('/admin/graphql', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3010')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With')
  if (req.method === 'OPTIONS') {
    res.sendStatus(200)
  } else {
    next()
  }
})

app.use('/admin/graphql',
  graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
  graphqlHTTP((request, response, params) => {
    return {
      schema,
      graphiql: true,
      //graphiql: __DEV__,
      rootValue: { request, params },
      pretty: __DEV__,
    }
  })
)

app.get('/', (req, res, next) => {
  res.redirect(301, '/admin')
})

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
  try {
    // 管理者認証
    if (req.path !== loginPath && !req.isAuthenticated()) {
      res.redirect(302, loginPath)
      next()
    }

    const css = new Set()

    // Enables critical path CSS rendering
    // https://github.com/kriasoft/isomorphic-style-loader
    const insertCss = (...styles) => {
      // eslint-disable-next-line no-underscore-dangle
      styles.forEach(style => css.add(style._getCss()))
    }

    // Universal HTTP client
    const fetch = createFetch(nodeFetch, {
      baseUrl: config.api.serverUrl,
      cookie: req.headers.cookie,
      schema,
      graphql,
    })

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      fetch,
      // The twins below are wild, be careful!
      pathname: req.path,
      query: req.query,
      flash: req.flash()
    }

    const route = await router.resolve(context)

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect)
      return
    }

    const data = { ...route }
    data.children = ReactDOM.renderToString(
      <App context={context} insertCss={insertCss}>
        {route.component}
      </App>
    )
    data.styles = [{ id: 'css', cssText: [...css].join('') }]

    const scripts = new Set()
    const addChunk = chunk => {
      if (chunks[chunk]) {
        chunks[chunk].forEach(asset => scripts.add(asset))
      } else if (__DEV__) {
        throw new Error(`Chunk with name '${chunk}' cannot be found`)
      }
    }
    addChunk('client')
    if (route.chunk) addChunk(route.chunk)
    if (route.chunks) route.chunks.forEach(addChunk)

    data.scripts = Array.from(scripts)
    data.app = {
      apiUrl: config.api.clientUrl,
    }

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />)
    res.status(route.status || 200)
    res.send(`<!doctype html>${html}`)
  } catch (err) {
    next(err)
  }
})

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError()
pe.skipNodeFiles()
pe.skipPackage('express')

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.error(pe.render(err))
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      styles={[{ id: 'css', cssText: errorPageStyle._getCss() }]} // eslint-disable-line no-underscore-dangle
    >
      {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err} />)}
    </Html>
  )
  res.status(err.status || 500)
  res.send(`<!doctype html>${html}`)
})

//
// Launch the server
// -----------------------------------------------------------------------------
const promise = models.sync().catch(err => console.error(err.stack))
if (!module.hot) {
  promise.then(() => {
    app.listen(config.port, () => {
      console.info(`The server is running at http://localhost:${config.port}/`)
    })
  })
}

//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module.hot) {
  app.hot = module.hot
  module.hot.accept('./router')
}

export default app
