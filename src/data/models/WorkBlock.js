import DataType from 'sequelize'
import Model from '../sequelize'

/* eslint-disable no-undef */
const WorkBlock = Model.define('WorkBlock', {
  id: {
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  workId: {
    type: DataType.INTEGER,
    allowNull: false,
  },

  body: {
    type: DataType.STRING,
  },

  path: {
    type: DataType.STRING,
  },

  extension: {
    type: DataType.STRING,
  },

  isPortrait: {
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },

  displayOrder: {
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 1,
  },

  createdAt: {
    type: DataType.DATE,
    field: 'created_at',
  },

  updatedAt: {
    type: DataType.DATE,
    field: 'updated_at',
  },
})

export default WorkBlock
