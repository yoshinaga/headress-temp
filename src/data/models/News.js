import DataType from 'sequelize'
import Model from '../sequelize'

/* eslint-disable no-undef */
const News = Model.define('News', {
  id: {
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataType.STRING,
    allowNull: false,
  },
  date: {
    type: DataType.DATE,
    allowNull: false,
  },
  category: {
    type: DataType.STRING,
    allowNull: false,
    defaultValue: '',
  },
  body: {
    type: DataType.STRING,
    allowNull: false,
  },
  url: {
    type: DataType.STRING,
    allowNull: false,
    defaultValue: '',
  },
  isPublished: {
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  createdAt: {
    type: DataType.DATE,
    field: 'created_at',
  },
  updatedAt: {
    type: DataType.DATE,
    field: 'updated_at',
  },
})

export default News
