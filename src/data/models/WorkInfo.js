import DataType from 'sequelize'
import Model from '../sequelize'

/* eslint-disable no-undef */
const WorkInfo = Model.define('WorkInfo', {
  id: {
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  workId: {
    type: DataType.INTEGER,
    allowNull: false,
  },

  key: {
    type: DataType.STRING,
    allowNull: false,
  },

  value: {
    type: DataType.STRING,
    allowNull: false,
  },

  displayOrder: {
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 1,
  },

  createdAt: {
    type: DataType.DATE,
    field: 'created_at',
  },

  updatedAt: {
    type: DataType.DATE,
    field: 'updated_at',
  },
})

export default WorkInfo
