import bcrypt from 'bcrypt'
import DataType from 'sequelize'
import Model from '../sequelize'

const Admin = Model.define(
  'Admin',
  {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },

    loginId: {
      type: DataType.STRING(255),
      allowNull: false,
      unique: true,
    },

    password: {
      type: DataType.STRING(255),
      allowNull: false,
    },
  },
  {
    indexes: [{ fields: ['login_id'] }],
  }
)

// 暗号化
Admin.prototype.setPassword = function (password) {
  const hash = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
  this.password = hash
  return hash
}

// パスワード照合
Admin.prototype.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password)
}

export default Admin
