import Admin from './Admin'
import sequelize from '../sequelize'
import Link from './Link'
import User from './User'
import UserLogin from './UserLogin'
import UserClaim from './UserClaim'
import UserProfile from './UserProfile'
import News from './News'
import Work from './Work'
import WorkInfo from './WorkInfo'
import WorkBlock from './WorkBlock'
import Setting from './Setting'

User.hasMany(UserLogin, {
  foreignKey: 'userId',
  as: 'logins',
  onUpdate: 'cascade',
  onDelete: 'cascade',
})

User.hasMany(UserClaim, {
  foreignKey: 'userId',
  as: 'claims',
  onUpdate: 'cascade',
  onDelete: 'cascade',
})

User.hasOne(UserProfile, {
  foreignKey: 'userId',
  as: 'profile',
  onUpdate: 'cascade',
  onDelete: 'cascade',
})

Work.hasMany(WorkInfo, {
  foreignKey: 'workId',
  as: 'infos',
  onUpdate: 'cascade',
  onDelete: 'cascade',
})

export const Blocks = Work.hasMany(WorkBlock, {
  foreignKey: 'workId',
  as: 'blocks',
  onUpdate: 'cascade',
  onDelete: 'cascade',
})

function sync(...args) {
  return sequelize.sync(...args)
}

export default { sync }
export { Admin, User, UserLogin, UserClaim, UserProfile, News, Work, WorkInfo, WorkBlock, Link, Setting }
