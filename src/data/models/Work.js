import DataType from 'sequelize'
import Model from '../sequelize'

/* eslint-disable no-undef */
const Work = Model.define('Work', {
  id: {
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  title: {
    type: DataType.STRING,
    allowNull: false,
  },

  category: {
    type: DataType.STRING,
    allowNull: false,
  },

  year: {
    type: DataType.INTEGER,
  },

  description: {
    type: DataType.STRING,
  },

  address: {
    type: DataType.STRING,
  },

  isPublished: {
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },

  path: {
    type: DataType.STRING,
  },

  extension: {
    type: DataType.STRING,
  },

  isPortrait: {
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },

  createdAt: {
    type: DataType.DATE,
    field: 'created_at',
  },

  updatedAt: {
    type: DataType.DATE,
    field: 'updated_at',
  },
})

export default Work
