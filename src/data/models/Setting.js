import DataType from 'sequelize'
import Model from '../sequelize'

/* eslint-disable no-undef */
const Setting = Model.define('Setting', {
  id: {
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  key: {
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  },

  value: {
    type: DataType.STRING,
  },

  createdAt: {
    type: DataType.DATE,
    field: 'created_at',
  },

  updatedAt: {
    type: DataType.DATE,
    field: 'updated_at',
  },
})

export default Setting
