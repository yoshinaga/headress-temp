# News
ニュース

# Queries
```
query($id: Int!) {
  news(id: $id) {
    id
    title
    isPublished
  }
}

{
  "id": 3,
}
```

# Mutations
```
mutation($input: NewsInput!) {
  createNews(input: $input) {
    id
    title
    body
  }
}

{
  "input": {"title": "VAR", "body": "VAR"}
}
```

```
mutation($id: Int! $input: NewsInput!) {
  updateNews(id: $id input: $input) {
    id
    title
    body
  }
}

{
  "id": 1,
  "input": {"title": "VAR", "body": "VAR"}
}
```

```
mutation($id: Int!) {
  deleteNews(id: $id)
}

{
  "id": 1,
}
```
