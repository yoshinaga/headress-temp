import { GraphQLInt, GraphQLNonNull, GraphQLBoolean } from 'graphql'
import { NewsType, NewsInput } from './NewsTypes'
import { News } from '../models'
import Rule from '../../lib/Rule'

const rule = new Rule({
  title: ['required', 'max:string'],
  body: ['required', 'max:text'],
})

const NewsMutations = {
  createNews: {
    type: NewsType,
    args: {
      input: {
        type: new GraphQLNonNull(NewsInput),
      },
    },
    resolve: (_source, args) => {
      rule.validate(args.input)
      return News.create({
        title: args.input.title,
        category: args.input.category,
        date: args.input.date,
        body: args.input.body,
        url: args.input.url,
        is_published: args.input.is_published,
      }).then(news => {
        return news
      })
    },
  },
  updateNews: {
    type: NewsType,
    args: {
      id: { type: new GraphQLNonNull(GraphQLInt) },
      input: {
        type: new GraphQLNonNull(NewsInput),
      },
    },
    resolve: (_source, args) => {
      rule.validate(args.input)
      return News.findOne({
        where: { id: args.id },
      }).then(news => {
        if (!news) throw new Error('対象のニュースが見つかりません')
        Object.assign(news, args.input)
        return news.save()
      })
    },
  },
  deleteNews: {
    type: GraphQLBoolean,
    args: {
      id: { type: new GraphQLNonNull(GraphQLInt) },
    },
    resolve: (_source, args) => {
      return News.findOne({
        where: { id: args.id },
      }).then(news => {
        try {
          news.destroy()
          return true
        } catch (e) {
          return false
        }
      })
    },
  }
}

export default NewsMutations
