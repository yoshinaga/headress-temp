import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLBoolean,
  GraphQLNonNull,
  GraphQLInputObjectType,
  GraphQLInt
} from 'graphql'
import DateTimeType from '../types/DateTimeType'

export const NewsType = new GraphQLObjectType({
  name: 'News',
  description: 'News',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    title: { type: GraphQLString },
    category: { type: GraphQLString },
    date: { type: DateTimeType },
    body: { type: GraphQLString },
    url: { type: GraphQLString },
    isPublished: { type: GraphQLBoolean },
    updatedAt: { type: DateTimeType },
  },
})

export const NewsGroupType = new GraphQLObjectType({
  name: 'NewsGroup',
  fields: {
    category: { type: GraphQLString },
    body: { type: GraphQLInt },
    id: { type: GraphQLInt },
  },
})

export const NewsInput = new GraphQLInputObjectType({
  name: 'NewsInput',
  description: 'ユーザ作成・更新時の入力値',
  fields: () => ({
    title: {
      type: new GraphQLNonNull(GraphQLString),
    },
    category: {
      type: GraphQLString,
    },
    date: {
      type: GraphQLString,
    },
    body: {
      type: GraphQLString,
    },
    url: {
      type: GraphQLString,
    },
    isPublished: {
      type: GraphQLBoolean,
    },
    // role: {
    //   type: UserRole,
    //   defaultValue: UserRole.getValue('ACCOUNTANT').value,
    // },
  }),
});
