import { GraphQLList, GraphQLInt, GraphQLString, GraphQLObjectType } from 'graphql'
import { NewsType, NewsGroupType } from './NewsTypes'
import { News } from '../models'
import sequelize from '../sequelize'

// const perPage = 10

const NewsQueries = {
  news: {
    type: NewsType,
    args: {
      id: {
        type: GraphQLInt,
      },
    },
    resolve({ request }, args) {
      return News.findByPk(args.id, {
        //      attributes: ['id', 'title', 'updated_at'],
      })
    },
  },

  newsList: {
    type: new GraphQLList(NewsType),
    args: {
      limit: {
        type: GraphQLInt,
      },
      order: {
        type: GraphQLString,
      },
    },
    resolve({ request }, args) {
      return News.findAll({
        order: args.order ? [args.order] : [],
        limit: args.limit,
      })
    },
  },

  newsCategories: {
    type: new GraphQLList(NewsGroupType),
    args: {},
    resolve({ request }, args) {
      return News.findAll({
        attributes: [
          'category',
          [sequelize.fn('COUNT', sequelize.col('category')), 'id']
        ],
        group: 'category',
        order: [sequelize.literal('category = ""'), 'category']
      }).then()
    },
  },

  newsGroup: {
    type: new GraphQLList(NewsGroupType),
    args: {},
    resolve({ request }, args) {
      return News.findAll({
        attributes: [
          [sequelize.fn('DATE_FORMAT', sequelize.col('date'), '%Y'), 'category'],
          [sequelize.fn('DATE_FORMAT', sequelize.col('date'), '%m'), 'body'],
          [sequelize.fn('COUNT', sequelize.col('date')), 'id']
        ],
        group: sequelize.fn('DATE_FORMAT', sequelize.col('date'), '%Y%m')
      }).then()
    },
  }
}

export default NewsQueries
