import Sequelize, { Op } from 'sequelize'
import config from '../config'

let sequelize
if (config.databaseUrl) {
  sequelize = new Sequelize(config.databaseUrl, {
    operatorsAliases: Op,
    define: {
      freezeTableName: true,
    },
  })
} else {
  sequelize = new Sequelize(
    config.database.database,
    config.database.username,
    config.database.password,
    config.database
  )
}

export default sequelize
