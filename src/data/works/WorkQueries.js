import { GraphQLList, GraphQLInt, GraphQLString } from 'graphql'
import { WorkType } from './WorkTypes'
import { Work, WorkBlock } from '../models'

const WorkQueries = {
  work: {
    type: WorkType,
    args: {
      id: {
        type: GraphQLInt,
      },
    },
    resolve({ request }, args) {
      return Work.findByPk(args.id, {
        include: [
          {
            model: WorkBlock,
            as: 'blocks',
            separate: false
          },
        ]
      })
    },
  },
  workList: {
    type: new GraphQLList(WorkType),
    args: {
      limit: {
        type: GraphQLInt,
      },
      order: {
        type: GraphQLString,
      },
    },
    resolve({ request }, args) {
      return Work.findAll({
        limit: args.limit,
        include: [
          {
            model: WorkBlock,
            as: 'blocks',
            separate: false
          },
        ]
      })
    },
  }
}

export default WorkQueries
