import { GraphQLInt, GraphQLNonNull, GraphQLBoolean, GraphQLList } from 'graphql'
import { GraphQLUpload } from 'graphql-upload'
import { WorkType, WorkInput } from './WorkTypes'
import { Work, WorkBlock } from '../models'
import Rule from '../../lib/Rule'
import Storage from '../../lib/Storage'

const rule = new Rule({
  title: ['required', 'max:string'],
})

/**
 * ブロックを新規作成・更新
 *
 * @param {Work} work
 * @param {WorkBlock} block
 * @param {Object} inputBlock
 */
const upsertWorkBlock = async (work, block, inputBlock) => {
  // ファイルデータ作成
  const { src, extension, isDeleted } = await Storage.parseUploadedFile(inputBlock.file)

  if (src) {
    // ID取得のため、あらかじめレコードを登録する
    if (!block) block = await work.createBlock(inputBlock)

    inputBlock.path = `blocks/${work.id}/${block.id}.${extension}`
    inputBlock.extension = extension
  } else if (isDeleted) {
    inputBlock.path = null
    inputBlock.extension = null
  }

  if (!block) {
    // 新規作成
    /** @todo addBlockすること */
    block = work.createBlock(inputBlock)
    work.blocks.push(block)
  } else {
    // 更新
    rule.fill(block, inputBlock)
    block.save()
  }

  // ファイル登録
  if (src) {
    Storage.put(src, inputBlock.path)
  } else if (isDeleted) {
    Storage.delete(block.path)
  }

  return block
}

/**
 * ブロックを削除
 *
 * @param {Work} work
 * @param {WorkBlock} block
 */
const deleteWorkBlock = async (work, block) => {
  Storage.delete(block.path)
  work.blocks.some((workBlock, index) => {
    if (workBlock.id === block.id) {
      work.blocks.splice(index, 1)
      return true
    }
  })

  return await block.destroy()
}

const WorkMutations = {
  upsertWork: {
    type: WorkType,
    description: '作品と作品ブロックの新規登録・更新',
    args: {
      id: { type: GraphQLInt },
      input: {
        type: new GraphQLNonNull(WorkInput),
      },
    },
    async resolve(_source, { id, input }) {
      input.blocks = input.blocks || []
      rule.validate(input)

      /** Work */
      let work
      /** Promise */
      const promises = []
      /** データベースに登録されているブロック (更新・削除用) */
      const oldBlocks = {}

      const include = [{
        model: WorkBlock,
        as: 'blocks',
        separate: false,
      }]

      if (id) {
        // Work 更新
        work = await Work.findOne({ where: { id }, include })
        if (!work) throw new Error('対象が見つかりません')

        work.blocks.forEach((block) => {
          oldBlocks[block.id] = block
        })
      } else {
        // Work 新規作成
        input.blocks.forEach((block, index) => {
          input.blocks[index].id = null
        })
        work = await Work.create(input, { include })
      }

      // Work ファイルデータ作成
      const { src, extension, isDeleted } = await Storage.parseUploadedFile(input.file)
      if (src) {
        input.path = `works/${work.id}.${extension}`
        input.extension = extension
      } else if (isDeleted) {
        input.path = null
        input.extension = null
      }

      // Work データ更新
      rule.fill(work, input)
      await work.save()

      // Work ファイル登録
      if (src) {
        Storage.put(src, input.path)
      } else if (isDeleted) {
        Storage.delete(input.path)
      }

      // WorkBlock 新規作成・更新
      input.blocks.forEach((inputBlock, index) => {
        const block = oldBlocks[inputBlock.id] || null
        if (block) {
          delete oldBlocks[inputBlock.id]
        }
        inputBlock.displayOrder = index + 1
        promises.push(upsertWorkBlock(work, block, inputBlock))
      })

      // WorkBlock 削除
      Object.keys(oldBlocks).forEach((blockId) => {
        const block = oldBlocks[blockId]
        promises.push(deleteWorkBlock(work, block))
      })

      await Promise.all(promises)
      return work
    },
  },
  deleteWork: {
    type: GraphQLBoolean,
    description: '作品と作品ブロックの削除',
    args: {
      id: { type: new GraphQLNonNull(GraphQLInt) },
    },
    async resolve(_source, args) {
      const work = await Work.findOne({
        where: { id },
      })

      try {
        let promises = []
        work.blocks.forEach(block => {
          promises.push(deleteWorkBlock(work, block))
        })
        Promise.all(promises)
        work.destroy()
        return true
      } catch (e) {
        return false
      }
    },
  },
  uploadBlockImage: {
    type: GraphQLBoolean,
    args: {
      file: {
        description: 'アップロードされたファイル',
        type: GraphQLUpload,
      },
    },
    async resolve(parent, { file }) {
      const { mimetype, createReadStream } = await file
      return await Storage.put(createReadStream(), 'blocks/filename.jpg')
    },
  },
  uploadBlockImages: {
    type: GraphQLBoolean,
      args: {
      files: {
        description: 'アップロードされた複数のファイル',
          type: new GraphQLList(GraphQLUpload),
      },
    },
    async resolve(parent, { files }) {
      const promises = []
      files.forEach(async (file, index) => {
        if (!file) return
        const { mimetype, createReadStream } = await file
        const displaySort = index + 1
        promises.push(Storage.put(createReadStream(), `blocks/${displaySort}/filename.jpg`))
      })

      return await Promise.all(promises)
    },
  },
}

export default WorkMutations
