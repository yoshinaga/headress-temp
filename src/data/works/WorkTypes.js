import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull,
  GraphQLInputObjectType,
  GraphQLList
} from 'graphql'
import { GraphQLUpload } from 'graphql-upload'
import DateTimeType from '../types/DateTimeType'

export const WorkInfoType = new GraphQLObjectType({
  name: 'WorkInfo',
  description: 'WorkInfo',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    workId: { type: GraphQLInt },
    key: { type: GraphQLString },
    value: { type: GraphQLString },
    displayOrder: { type: GraphQLInt },
    createdAt: { type: DateTimeType },
    updatedAt: { type: DateTimeType },
  },
})

export const WorkBlockType = new GraphQLObjectType({
  name: 'WorkBlock',
  description: 'WorkBlock',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    workId: { type: GraphQLInt },
    body: { type: GraphQLString },
    path: { type: GraphQLString },
    extension: { type: GraphQLString },
    isPortrait: { type: GraphQLBoolean },
    isPublished: { type: GraphQLBoolean },
    displayOrder: { type: GraphQLInt },
    createdAt: { type: DateTimeType },
    updatedAt: { type: DateTimeType },
  },
})

export const WorkType = new GraphQLObjectType({
  name: 'Work',
  description: 'Work',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    title: { type: GraphQLString },
    category: { type: GraphQLString },
    year: { type: GraphQLInt },
    address: { type: GraphQLString },
    description: { type: GraphQLString },
    isPublished: { type: GraphQLBoolean },
    path: { type: GraphQLString },
    extension: { type: GraphQLString },
    isPortrait: { type: GraphQLBoolean },
    createdAt: { type: DateTimeType },
    updatedAt: { type: DateTimeType },
    infos: { type: new GraphQLList(WorkInfoType) },
    blocks: { type: new GraphQLList(WorkBlockType) },
  },
})

export const WorkBlockInput = new GraphQLInputObjectType({
  name: 'WorkBlockInput',
  description: 'ブロックの入力値',
  fields: () => ({
    id: { type: GraphQLInt },
    body: { type: GraphQLString },
    isPublished: { type: GraphQLBoolean },
    displayOrder: { type: GraphQLInt },
    file: { type: GraphQLUpload },
    isPortrait: { type: GraphQLBoolean },
  }),
})

export const WorkInput = new GraphQLInputObjectType({
  name: 'WorkInput',
  description: '作成・更新時の入力値',
  fields: () => ({
    title: { type: new GraphQLNonNull(GraphQLString) },
    category: { type: GraphQLString },
    year: { type: GraphQLInt },
    address: { type: GraphQLString },
    description: { type: GraphQLString },
    isPublished: { type: GraphQLBoolean },
    file: { type: GraphQLUpload },
    isPortrait: { type: GraphQLBoolean },
    blocks: { type: new GraphQLList(WorkBlockInput) },
  }),
})
