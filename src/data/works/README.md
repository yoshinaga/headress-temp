# Work
記事

# Examples
```
query {
  work(id: 1) {
    id
    title
    year
    description
    isPublished
    createdAt
    updatedAt
  }
}

query {
  workList {
    id
    title
    blocks {
      id
      path
    }
  }
}
```

```
mutation($id: Int $input: WorkInput!) {
  upsertWork(id: $id input: $input) {
    id
    title
    blocks {
      body path
    }
  }
}

{
  "input": {
    "title": "VAR",
    "category": "CATEGORY A",
    "blocks": [
      {
        "body": "QUERY VAR"
      }
    ]
  }
}

{
  "id": 1,
  "input": {
    "title": "VAR",
    "blocks": [
      {
        "id": 2,
        "body": "QUERY VAR"
      }
    ]
  }
}
input.blocks.0.file
```

```
mutation($id: Int!) {
  deleteWork(id: $id)
}

{
  "id": 1,
}
```

```
mutation($file: Upload!) {
  uploadBlockImage(file: $file)
}

{
  file: File
}
```
