import { GraphQLNonNull, GraphQLBoolean } from 'graphql'
import { MessageInput } from './MessageTypes'
import Rule from '../../lib/Rule'
import dayjs from 'dayjs'
import nodemailer from 'nodemailer'


const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  secure: process.env.MAIL_SECURE,
  auth: {
    type: 'OAuth2',
    clientId: process.env.MAIL_API_CLIENT_ID,
    clientSecret: process.env.MAIL_API_CLIENT_SECRET,
  },
})

const rule = new Rule({
  body: ['required', 'max:10', 'max:text'],
  name: ['required', 'max:10', 'max:string'],
  phone: ['nullable', 'phone'],
  email: ['nullable', 'email', 'max:string'],
  zip: ['nullable', 'zip'],
  address: ['nullable', 'max:text'],
  title: ['empty'],
  requestedAt: ['required', 'datetime'],
  confirmedAt: ['required', 'datetime', 'after:requestedAt', (attribute, value, input) => {
    const now = dayjs()
    const hoursFromRequest = now.diff(dayjs(input['requestedAt']))
    const hoursFromConfirm = now.diff(dayjs(input['confirmedAt']))

    if (hoursFromRequest >= 86400000) {
      // 入力フォームページを開いてから送信までに24時間経過
      return 'リクエストの期限が切れました'
    } else if (hoursFromRequest < 20000 || hoursFromConfirm < 0) {
      // 入力フォームページを開いてから20秒以内に送信
      return '不正なリクエストです'
    }
  }],
})

const sendMail = (input) => {
  console.log(input)

  const body = `
  ${input.name}
  ${input.email}

  ${input.body}
  `

  const data = {
    from: 'from@example.com',
    to: process.env.MAIL_TO_ADDRESS,
    text: "テキストメール本文\nテキストメール本文\nテキストメール本文",
    html: 'HTMLメール本文<br>HTMLメール本文<br>HTMLメール本文',
    subject: 'メール件名',
  }

  transporter.sendMail(data, (error, info) => {
    if (error) {
      console.log(error); // エラー情報
    } else {
      console.log(info);  // 送信したメールの情報
    }
  });

  return true
}

const MessageMutations = {
  createMessage: {
    type: GraphQLBoolean,
    description: 'メッセージをメール送信',
    args: {
      input: {
        type: new GraphQLNonNull(MessageInput),
      },
    },
    resolve: (_source, args) => {
      rule.validate(args.input)


      return sendMail(args.input)
    },
  },
}

export default MessageMutations
