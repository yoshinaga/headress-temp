import {
  GraphQLString,
  GraphQLNonNull,
  GraphQLInputObjectType,
} from 'graphql'

export const MessageInput = new GraphQLInputObjectType({
  name: 'MessageInput',
  description: 'メッセージ送信',
  fields: () => ({
    title: {
      type: GraphQLString,
    },
    body: {
      type: new GraphQLNonNull(GraphQLString),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    phone: {
      type: GraphQLString,
    },
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    zip: {
      type: GraphQLString,
    },
    address: {
      type: GraphQLString,
    },
    requestedAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    confirmedAt: {
      type: GraphQLString,
    },
  }),
})
