import { GraphQLList, GraphQLInt, GraphQLString, GraphQLObjectType } from 'graphql'
import { LinkType, LinkGroupType } from './LinkTypes'
import { Link } from '../models'
import sequelize from '../sequelize'

// const perPage = 10

const LinkQueries = {
  link: {
    type: LinkType,
    args: {
      id: {
        type: GraphQLInt,
      },
    },
    resolve({ request }, args) {
      return Link.findByPk(args.id, {
        //      attributes: ['id', 'title', 'updated_at'],
      })
    },
  },

  linkList: {
    type: new GraphQLList(LinkType),
    args: {
      limit: {
        type: GraphQLInt,
      },
      order: {
        type: GraphQLString,
      },
    },
    resolve({ request }, args) {
      return Link.findAll({
        order: args.order ? [args.order] : [],
        limit: args.limit,
      })
    },
  },

  linkCategories: {
    type: new GraphQLList(LinkGroupType),
    args: {},
    resolve({ request }, args) {
      return Link.findAll({
        attributes: [
          'category',
          [sequelize.fn('COUNT', sequelize.col('category')), 'id']
        ],
        group: 'category',
        order: [sequelize.literal('category = ""'), 'category']
      }).then()
    },
  },
}

export default LinkQueries
