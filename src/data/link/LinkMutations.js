import { GraphQLInt, GraphQLNonNull, GraphQLBoolean } from 'graphql'
import { LinkType, LinkInput } from './LinkTypes'
import { Link } from '../models'
import Rule from '../../lib/Rule'

const rule = new Rule({
  title: ['required', 'max:string'],
  body: ['required', 'max:text'],
})

const LinkMutations = {
  upsertLink: {
    type: LinkType,
    args: {
      id: { type: GraphQLInt },
      input: {
        type: new GraphQLNonNull(LinkInput),
      },
    },
    async resolve(_source, { id, input }) {
      rule.validate(input)

      if (id) {
        // 更新
        const link = await Link.findOne({ where: { id } })
        if (!link) throw new Error('対象が見つかりません')
        Object.assign(link, input)
        return link.save()
      } else {
        // 新規作成
        return await Link.create(input)
      }
    },
  },
  deleteLink: {
    type: GraphQLBoolean,
    args: {
      id: { type: new GraphQLNonNull(GraphQLInt) },
    },
    resolve: (_source, args) => {
      return Link.findOne({
        where: { id: args.id },
      }).then(link => {
        try {
          link.destroy()
          return true
        } catch (e) {
          return false
        }
      })
    },
  }
}

export default LinkMutations
