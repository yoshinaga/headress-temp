import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLBoolean,
  GraphQLNonNull,
  GraphQLInputObjectType,
  GraphQLInt
} from 'graphql'
import DateTimeType from '../types/DateTimeType'

export const LinkType = new GraphQLObjectType({
  name: 'Link',
  description: 'Link',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    title: { type: GraphQLString },
    category: { type: GraphQLString },
    description: { type: GraphQLString },
    url: { type: GraphQLString },
    isPublished: { type: GraphQLBoolean },
    updatedAt: { type: DateTimeType },
  },
})

export const LinkGroupType = new GraphQLObjectType({
  name: 'LinkGroup',
  fields: {
    category: { type: GraphQLString },
    body: { type: GraphQLInt },
    id: { type: GraphQLInt },
  },
})

export const LinkInput = new GraphQLInputObjectType({
  name: 'LinkInput',
  description: 'ユーザ作成・更新時の入力値',
  fields: () => ({
    title: {
      type: new GraphQLNonNull(GraphQLString),
    },
    category: {
      type: GraphQLString,
    },
    description: {
      type: GraphQLString,
    },
    url: {
      type: GraphQLString,
    },
    isPublished: {
      type: GraphQLBoolean,
    },
  }),
});
