# Link
ニュース

# Queries
```
query($id: Int!) {
  link(id: $id) {
    id
    title
    isPublished
  }
}

{
  "id": 3,
}
```

# Mutations
```
mutation($id: Int! $input: LinkInput!) {
  upsertLink(id: $id input: $input) {
    id
    title
    body
  }
}

{
  "id": 1,
  "input": {
    "title": "VAR", "description": "VAR", "url": "https://example.com"
  }
}
```

```
mutation($id: Int!) {
  deleteLink(id: $id)
}

{
  "id": 1,
}
```
