/** アプリケーション共通の項目名 */
export default {
  body: 'お問い合わせ内容',
  name: 'お名前',
  tel: '電話番号',
  email: 'E-mail',
  zip: '郵便番号',
  address: '住所',

  // Message
  requestedAt: 'リクエスト時間',
  confirmedAt: '確認時間',
}
