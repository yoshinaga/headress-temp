import dayjs from 'dayjs'
import { GraphQLList, GraphQLString } from 'graphql'
import Sequelize, { Op } from 'sequelize'
import { Work, Link, Setting } from '../models'
import ContentType from '../types/ContentType'

const contentQueries = {
  changedContents: {
    type: new GraphQLList(ContentType),
    description: '未公開のコンテンツ一覧',
    args: {
      publishedAt: {
        type: GraphQLString,
      },
    },
    async resolve({ request }, args) {
      const publishedAt = args.publishedAt || await Setting.findOne({
        where: { key: 'publishedAt' }
      }).then((setting) => setting ? setting.value : dayjs())
      const where = { updatedAt: { [Op.gt]: publishedAt } }
      const promises = []

      // Work
      promises.push(Work.findAll({ where }).then((works) => {
        return works.map((work) => {
          return {
            type: 'Work',
            name: work.title,
            createdAt: work.createdAt,
            updatedAt: work.updatedAt,
          }
        })
      }))

      // Link
      promises.push(Link.findAll({ where }).then((links) => {
        return links.map((link) => {
          return {
            type: 'Link',
            name: link.title,
            createdAt: link.createdAt,
            updatedAt: link.updatedAt,
          }
        })
      }))

      // Setting
      promises.push(Setting.findAll({ where }).then((settings) => {
        return settings.map((setting) => {
          return {
            type: 'Setting',
            name: setting.key.replace(/^[a-z]/, (initial) => initial.toUpperCase()),
            createdAt: setting.createdAt,
            updatedAt: setting.updatedAt,
          }
        })
      }))

      let contents = []
      await Promise.all(promises).then((promises) => {
        promises.forEach((appends) => {
          contents = contents.concat(appends)
        })
      })
      return contents
    },
  },
}

export default contentQueries
