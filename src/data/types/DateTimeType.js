import { GraphQLScalarType } from 'graphql'
import dayjs from 'dayjs'

// 日付型
let DateTimeType = new GraphQLScalarType({
  name: 'DateTime',
  serialize: (value) => {
    return dayjs(value).format('YYYY/M/D')
  },
  parseValue: (value) => {
    let dateTime = new Date(value)
    return Number.isNaN(dateTime.getTime()) ? null : dateTime
  },
  parseLiteral: (ast) => {
    return ast.kind === Kind.STRING ? parseDate(ast.value) : null
  },
})

export default DateTimeType
