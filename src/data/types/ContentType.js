import {
  GraphQLObjectType as ObjectType,
  GraphQLID as ID,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
} from 'graphql'
import DateTimeType from '../types/DateTimeType'

const ContentType = new ObjectType({
  name: 'Content',
  fields: {
    type: { type: StringType },
    name: { type: StringType },
    createdAt: { type: DateTimeType },
    updatedAt: { type: DateTimeType },
  },
})

export default ContentType
