/** Sequelize Config */
module.exports = {
  database: process.env.DB_DATABASE,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  dialect: process.env.DB_DIALECT || 'mariadb',
  dialectOptions: {
    useUTC: false,
    timezone: 'Etc/GMT-9',
  },
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || '63306',
  timezone: 'Asia/Tokyo',
  define: {
    underscored: true,
    charset: 'utf8',
    collate: 'utf8_general_ci',
    timestamps: true
  },
  logging: !!process.env.DB_LOGGING
}
