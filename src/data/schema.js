import { GraphQLSchema, GraphQLObjectType } from 'graphql'

import me from './queries/me'
import feed from './queries/feed'
import NewsQueries from './news/NewsQueries'
import NewsMutations from './news/NewsMutations'
import WorkQueries from './works/WorkQueries'
import WorkMutations from './works/WorkMutations'
import LinkQueries from './link/LinkQueries'
import LinkMutations from './link/LinkMutations'
import AdminQueries from './admin/AdminQueries'
import AdminMutations from './admin/AdminMutations'
import SettingQueries from './setting/SettingQueries'
import SettingMutations from './setting/SettingMutations'
import ContentQueries from './queries/ContentQueries'
import MessageMutations from './message/MessageMutations'

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'Query',
    fields: {
      me,
      feed,
      ...NewsQueries,
      ...WorkQueries,
      ...LinkQueries,
      ...AdminQueries,
      ...SettingQueries,
      ...ContentQueries,
    },
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      ...NewsMutations,
      ...WorkMutations,
      ...LinkMutations,
      ...AdminMutations,
      ...SettingMutations,
      ...MessageMutations,
    },
  }),
})

export default schema
