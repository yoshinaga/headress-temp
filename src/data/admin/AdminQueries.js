import { GraphQLList, GraphQLInt, GraphQLString, GraphQLObjectType } from 'graphql'
import { AdminType, AdminGroupType } from './AdminTypes'
import { Admin } from '../models'
import sequelize from '../sequelize'

const AdminQueries = {
  admin: {
    type: AdminType,
    args: {
      id: {
        type: GraphQLInt,
      },
    },
    resolve({ request }, args) {
      return Admin.findByPk(args.id, {
        //      attributes: ['id', 'title', 'updated_at'],
      })
    },
  },

  admins: {
    type: new GraphQLList(AdminType),
    args: {
      limit: {
        type: GraphQLInt,
      },
      order: {
        type: GraphQLString,
      },
    },
    resolve({ request }, args) {
      return Admin.findAll({
        order: args.order ? [args.order] : [],
        limit: args.limit,
      })
    },
  },
}

export default AdminQueries
