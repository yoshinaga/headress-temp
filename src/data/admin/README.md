# Link
ニュース

# Queries
```
query($id: Int!) {
  link(id: $id) {
    id
    title
    isPublished
  }
}

{
  "id": 3,
}
```

# Mutations
```
mutation($input: AdminInput!) {
  upsertAdmin(input: $input) {
    id
    loginId
    password
  }
}

{
  "input": {
    "loginId": "test",
    "password": "test"
  }
}
```

```
mutation($id: Int!) {
  deleteLink(id: $id)
}

{
  "id": 1,
}
```
