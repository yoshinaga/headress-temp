import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInputObjectType,
} from 'graphql'

export const AdminType = new GraphQLObjectType({
  name: 'Admin',
  description: 'Admin',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    loginId: { type: GraphQLString },
    password: { type: GraphQLString },
  },
})

export const AdminInput = new GraphQLInputObjectType({
  name: 'AdminInput',
  description: '作成・更新時の入力値',
  fields: () => ({
    loginId: {
      type: new GraphQLNonNull(GraphQLString),
    },
    password: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});
