import { GraphQLInt, GraphQLNonNull, GraphQLBoolean } from 'graphql'
import { AdminType, AdminInput } from './AdminTypes'
import { Admin } from '../models'
import Rule from '../../lib/Rule'

const rule = new Rule({
  loginId: ['required', 'max:string'],
})

const AdminMutations = {
  upsertAdmin: {
    type: AdminType,
    args: {
      id: { type: GraphQLInt },
      input: {
        type: new GraphQLNonNull(AdminInput),
      },
    },
    async resolve(_source, { id, input }) {
      rule.validate(input)

      if (id) {
        // 更新
        const admin = await Admin.findOne({ where: { id } })
        if (!admin) throw new Error('対象が見つかりません')

        rule.fill(admin, input)
        if (input.password) {
          admin.setPassword(input.password)
        }
        return admin.save()
      } else {
        // 新規作成
        return await Admin.create(input)
      }
    },
  },
  deleteAdmin: {
    type: GraphQLBoolean,
    args: {
      id: { type: new GraphQLNonNull(GraphQLInt) },
    },
    resolve: (_source, args) => {
      return Admin.findOne({
        where: { id: args.id },
      }).then(admin => {
        try {
          admin.destroy()
          return true
        } catch (e) {
          return false
        }
      })
    },
  }
}

export default AdminMutations
