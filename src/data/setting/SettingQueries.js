import { GraphQLList, GraphQLString } from 'graphql'
import { SettingType } from './SettingTypes'
import { Setting } from '../models'

const SettingQueries = {
  setting: {
    type: SettingType,
    args: {
      key: {
        type: GraphQLString,
      },
    },
    resolve({ request }, { key }) {
      return Setting.findOne({ where: { key } })
    },
  },

  settings: {
    type: new GraphQLList(SettingType),
    args: {
      keys: {
        type: new GraphQLList(GraphQLString),
      },
    },
    resolve({ request }, { keys }) {
      return Setting.findAll({ where: keys ? { key: keys } : {} })
    },
  },
}

export default SettingQueries
