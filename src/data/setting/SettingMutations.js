import { GraphQLInt, GraphQLNonNull, GraphQLBoolean, GraphQLString, GraphQLList } from 'graphql'
import { SettingType, SettingInput } from './SettingTypes'
import { Setting } from '../models'
import Rule from '../../lib/Rule'

const rule = new Rule({
  title: ['required', 'max:string'],
  body: ['required', 'max:text'],
})

const SettingMutations = {
  upsertSetting: {
    type: SettingType,
    args: {
      input: {
        type: new GraphQLNonNull(SettingInput),
      },
    },
    async resolve(_source, { input }) {
      rule.validate(input)

      const setting = (await Setting.findOrCreate({
        where: { key: input.key }
      }))[0] || null
      if (!setting) throw new Error('対象が見つかりません')
      setting.value = input.value
      return setting.save()
    },
  },
  upsertSettings: {
    type: new GraphQLList(SettingType),
    args: {
      input: {
        type: new GraphQLNonNull(new GraphQLList(SettingInput)),
      },
    },
    async resolve(_source, { input }) {
      rule.validate(input)

      const keys = Object.keys(input).map((index) => {
        return input[index].key
      })
      const settings = await Setting.findAll({
        where: { key: keys }
      })

      const promises = input.map(({ key, value }) => {
        return Setting.findOrCreate({
          where: { key }
        }).then(([ setting ]) => {
          setting.value = value
          return setting.save()
        })
      })

      return await Promise.all(promises)
    },
  },
  deleteSetting: {
    type: GraphQLBoolean,
    args: {
      key: { type: new GraphQLNonNull(GraphQLString) },
    },
    resolve: (_source, { key }) => {
      return Setting.findOne({
        where: { key },
      }).then(setting => {
        try {
          setting.destroy()
          return true
        } catch (e) {
          return false
        }
      })
    },
  }
}

export default SettingMutations
