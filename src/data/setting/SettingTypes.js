import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInputObjectType,
} from 'graphql'
import DateTimeType from '../types/DateTimeType'

export const SettingType = new GraphQLObjectType({
  name: 'Setting',
  description: 'Setting',
  fields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    key: { type: new GraphQLNonNull(GraphQLID) },
    value: { type: GraphQLString },
    updatedAt: { type: DateTimeType },
  },
})

export const SettingInput = new GraphQLInputObjectType({
  name: 'SettingInput',
  description: '作成・更新時の入力値',
  fields: () => ({
    key: {
      type: new GraphQLNonNull(GraphQLString),
    },
    value: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});
