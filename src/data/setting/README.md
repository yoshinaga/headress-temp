# News
ニュース

# Queries
```
query($key: String!) {
  setting(key: $key) {
    key
    value
  }
}

{
  "key": "example"
}
```

```
query($key: String!) {
  settings(keys: $keys) {
    key
    value
  }
}

{
  "keys": ["example"]
}
```

```
{
  changedContents {
    type name createdAt updatedAt
  }
}
```

# Mutations
```
mutation($input: SettingInput!) {
  upsertSetting(input: $input) {
    key
    value
  }
}

{
  "input": {
    "key": "example",
    "value": "example value"
  }
}
```

```
mutation($input: [SettingInput]!) {
  upsertSettings(input: $input) {
    key
    value
  }
}

{
  "input": [
    {
      "key": "example",
      "value": "example value"
    },
    {
      "key": "alt",
      "value": "alternative value"
    }
  ]
}
```

```
mutation($key: String!) {
  deleteSetting(key: $key)
}

{
  "key": "example"
}
```