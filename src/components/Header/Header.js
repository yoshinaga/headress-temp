import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import s from './Header.css'
import Link from '../Link'
import Navigation from '../Navigation'
import { AppBar, Toolbar, IconButton, Typography, Button } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'

export default function Header() {
  useStyles(s)
  return (
    <div className={s.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit">
            <MenuIcon />
          </IconButton>
          <Link className={s.brand} to="/admin">
            admin
          </Link>
          <Navigation />
        </Toolbar>
      </AppBar>
    </div>
  )
}
