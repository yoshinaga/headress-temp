import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import Link from '../Link'
import s from './Feedback.css'

export default function Feedback() {
  useStyles(s)
  return (
    <div className={s.root}>
      <div className={s.container}>
        <Link className={s.link} to="/">
          Headless Contents Management System
        </Link>
      </div>
    </div>
  )
}
