import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import PropTypes from 'prop-types'

// external-global styles must be imported in your JS.
//import normalizeCss from 'normalize.css'
import Footer from '../Footer'
import Header from '../Header'
import s from './Layout.css'
import MuiTheme from '../../lib/MuiTheme'
import { ThemeProvider } from '@material-ui/styles'
import { CssBaseline, Container } from '@material-ui/core'
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert'
import { SnackbarProvider } from 'notistack'

export default function Layout({ children }) {
  useStyles(s)
  return (
    <ThemeProvider theme={MuiTheme}>
      <CssBaseline />
      <Header />
      <SnackbarProvider anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
        <Container className={s.content}>
          {children}
        </Container>
        <Footer />
      </SnackbarProvider>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}
