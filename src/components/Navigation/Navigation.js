import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import cx from 'classnames'
import s from './Navigation.css'
import Link from '../Link'

export default function Navigation() {
  useStyles(s)
  return (
    <div className={s.root} role="navigation">
      <Link className={s.link} to="/admin/works">
        Works
      </Link>
      <Link className={s.link} to="/admin/links">
        Links
      </Link>
      <Link className={s.link} to="/admin/setting">
        Setting
      </Link>
      <span className={s.spacer}> | </span>
      <Link className={cx(s.link, s.highlight)} to="/logut">
        Log out
      </Link>
    </div>
  )
}
