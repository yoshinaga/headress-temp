import useStyles from 'isomorphic-style-loader/useStyles'
import React from 'react'
import PropTypes from 'prop-types'
import s from './InputFile.css'

function handleChange(props, event) {
  if (props.onChange) {
    props.onChange(event)
  }
}

export default function InputFile(props) {
  const { name, backgroundImage, label } = props
  useStyles(s)

  return (
    <div className={s.root}>
      <div className={s.label}>
        {label}
      </div>
      <label className={[s.area]}>
        <input
          type="file"
          name={name}
          accept="image/*"
          className={s.input}
          onChange={e => handleChange(props, e)}
        />
        {backgroundImage
          ? <img className={s.image} src={backgroundImage} />
          : <div className={s.text}>ファイルを選択してください</div>
        }
      </label>
    </div>
  )
}

InputFile.propTypes = {
  name: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string,
  onClick: PropTypes.func,
}

InputFile.defaultProps = {
  onClick: null,
}
