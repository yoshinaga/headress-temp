import React from 'react'
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, Grow, FormControl } from '@material-ui/core'
import { gqlAction, gqlFetch } from '../lib/gqlFetch'
import fetch from 'node-fetch'
import { useSnackbar } from 'notistack'

function PublishButton({ onSuccess }) {
  const Transition = React.forwardRef(function Transition(props, ref) {
    return <Grow ref={ref} {...props} />
  })

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const publish = async () => {
    setOpen(false)

    enqueueSnackbar('公開を開始します...')
    const res = await fetch('/admin/publish', { method: 'POST' })

    gqlAction({
      query: `query($key: String!) {
        setting(key: $key) {
          value updatedAt
        }
      }`,
      variables: {
        key: 'published_at'
      },
      action: {
        notify: enqueueSnackbar,
        message: `変更が公開されました`,
        then: onSuccess()
      },
    })
  }

  return (<div>
    <div>
      <Button color="primary" onClick={handleClickOpen}>
        Publish
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Publish
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            すべての変更をサイトに反映しますか？
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus onClick={publish}>
            Publish
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  </div>)
}

export default PublishButton