/**
 * ドット区切りのキーでステートを深掘りして更新
 *
 * @param {Object} state
 * @param {String} name ドット区切りのstateキー
 * @param {*} data 更新する値
 */
export function updateDeepState(state, name, data) {
  const nameChunks = name.split('.')
  const lastIndex = nameChunks.length - 1
  if (name === '' || lastIndex === 0) return data

  let pointer = null
  state = { ...state }
  nameChunks.forEach((nameChunk, index) => {
    if (index !== lastIndex) {
      // 参照渡しで深掘りしていく
      const newPointer = pointer ? pointer[nameChunk] : state[nameChunk]
      if (newPointer !== undefined) {
        pointer = newPointer
      } else {
        // キーが存在しない場合はオブジェクトを作成
        pointer[nameChunk] = {}
        pointer = pointer[nameChunk]
      }
    } else if (typeof data === 'object' && typeof pointer[nameChunk] === 'object') {
      // 最後にオブジェクトを結合する
      Object.assign(pointer[nameChunk], data)
    } else {
      // 最後に代入する
      pointer[nameChunk] = data
    }
  })

  const firstNameChunk = nameChunks[0]
  return { [firstNameChunk]: state[firstNameChunk] }
}
