import React, { Fragment } from 'react'
import { Button, Menu, MenuItem } from '@material-ui/core'

export default function WorkBlockMenu({ index, actions, children }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  };

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleItemClick = (event) => {
    actions[event.currentTarget.dataset.name](index, event)
    handleClose()
  }

  return (
    <>
      <Button size="small" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        { children }
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        onClick={handleClose}
      >
        {Object.keys(actions).map((name, itemIndex) => {
          return actions[name] && <MenuItem key={itemIndex} onClick={handleItemClick} data-name={name}>{name}</MenuItem>
        })}
      </Menu>
    </>
  )
}
