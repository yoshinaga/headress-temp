import Exif from 'exif-js/exif.js';

export default class UploadedImage {
  constructor(options = {}) {
    this.width = options.width || 500
    this.sizeLimit = options.sizeLimit || 1200
    this.handleReaderLoad = this.handleReaderLoad.bind(this)
  }

  /** アップロード完了時に実行 */
  onUploaded(infos) {
    console.log(infos)
  }

  /* アップロードされた画像からプレビュー画像を作成 */
  setImage(event) {
    let files = event.target.files || e.dataTransfer.files
    if (!files.length) {
      return;
    }
    let file = files[0]
    let fileReader = new FileReader()

    fileReader.onload = this.handleReaderLoad
    fileReader.readAsDataURL(file)
  }

  /** プレビュー用の縮小画像を作成 */
  handleReaderLoad (event) {
    const image = new Image()
    const imageBase64 = event.target.result
    const imageType = imageBase64.substring(5, imageBase64.indexOf(';'))
    const rotate = this.getExifOrientation(imageBase64)

    image.onload = () => {
      // 縦横比を保存してプレビュー用に縮小
      const ratio = image.width / image.height
      const width = this.width
      const height = width / ratio

      // Exif情報に基づいて回転
      let canvas = document.createElement('canvas')
      if (rotate === 90 || rotate === 270) {
        canvas.width = height
        canvas.height = width
      } else {
        canvas.width = width
        canvas.height = height
      }
      const context = canvas.getContext('2d');
      if (0 < rotate && rotate < 360) {
        context.rotate(rotate * Math.PI / 180);
        if (rotate === 90) context.translate(0, -height)
        else if (rotate === 180) context.translate(-width, -height)
        else if (rotate === 270) context.translate(-width, 0)
      }
      context.drawImage(image, 0, 0, width, height)
      const imagePreview = canvas.toDataURL(imageType)
      const isPortrait = canvas.height > canvas.width
      const isOversize = canvas.width > this.sizeLimit || canvas.height > this.sizeLimit
      canvas = null

      // 完了時のコールバックを実行
      this.onUploaded({ imageBase64, imagePreview, isPortrait, isOversize })
    }

    image.src = imageBase64
  }

  /* Exif情報に基づいて回転角度を返す */
  getExifOrientation(imageBase64) {
    imageBase64 = imageBase64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
    const binaryString = atob(imageBase64);
    const len = binaryString.length;
    let bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    let exif = Exif.readFromBinaryFile(bytes.buffer);
    if (exif && exif.Orientation) {
      switch (exif.Orientation) {
        case 3: return 180;
        case 6: return 90;
        case 8: return -90;
        default: return 0;
      }
    }
  }
}
